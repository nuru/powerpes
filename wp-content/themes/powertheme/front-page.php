<?php
/**
* Template Name: PowerPes front-page Template
*/
?>


<?php get_header(); ?>


<div class="gr-top">CORRA - OS ESTOQUES ESTÃO ACABANDO!</div>
<div class="menu-top">
    <div>
        <a href="#"><img class="logo-header-img" src='<?php echo wp_get_attachment_image_src(18, 'medium')[0]; ?>'></a>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light nav-menu">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
        <a class="nav-item nav-link active" href="#">Home<span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link" href="#caracteristicas">Caracteristicas</a>
        <a class="nav-item nav-link" href="#beneficios">Como Funciona</a>
        <a class="nav-item nav-link" href="#testemunhos">Testemunhos</a>
        <a class="nav-item nav-link" href="#three_products">Comprar</a>
        </div>
    </div>
    </nav>

</div>





<div style=" width: 100%; max-height: 950px; background: url(<?php echo wp_get_attachment_image_src(134, 'full')[0]; ?> ); background-position: center; background-size: cover; background-repeat: no-repeat;">

    <h1 class="text-center head-txt"><span style="color:yellow;">ALIVIE E PROTEJA</span><br>
    OS SEUS PÉS HOJE!</h1>
    <div class="vid-tim-wrap">
        <div class="vid-tim">
            <div class="vid-frame">
                <iframe class="i-frame-vid"
                    width="1663" height="689" src="https://www.youtube.com/embed/TxlBeStvcrc" 
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
            </div>
        </div>

        <div class="vid-tim">
            <p class="tim-txt">4326 JÁ VENDIDOS.
                <br>
                TEMPO PARA FINALIZAR A 
                <br>
                OFERTA:
            </p>
            <div class="countdown text-center">
                <p class="timer">
                    <span class="timer-horas"></span>:<span class="timer-minutos"></span>:<span class="timer-segundos"></span>
                </p>
                <div>
                    <p> <span id="countdown" class="timers"></span></p></div>
            </div>
            <div class="progress pink lighten-4">
                <div class="determinate pink" style="width: 70%"></div>
            </div>
            <a href="#three_products" class="comprar">Comprar</a>

            
            
    
    
            

        </div>

    </div>
    
</div>

<div class="container text-center" style="max-width: 80%;">
    <section id="caracteristicas">
        <div>
            <h1 class="blue-text porque">PORQUE VOCÊ PRECISA DE SUPER SOLA</h1>
        </div>

        <div class="wrapper">
            <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(25, 'full')[0]; ?>' style="max-width: 290px;"></div>
                <div class="mar-top-btm">
                    <h3>ALIVIA A DOR NAS ARTICULAÇÕES</h3>
                    <p>PowerPes oferece uma combinação de absorção de impacto, 
                        acolchoamento e apoio para os seus pés, o que por sua vez 
                        pode ajudar a aliviar as dores no joelho e te ajudar a andar
                        com muito mais conforto.
                    </p>
                </div>
            </div>

            <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(23, 'full')[0]; ?>'style="max-width: 290px;"></div>
                <div class="mar-top-btm">
                    <h3>ABSORÇÃO TOTAL</h3>
                    <p>Absorção de Impacto & Apoio dos Pés a Cada Passo</p>
                </div>
            </div>

            <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(24, 'full')[0]; ?>' style="max-width: 290px;"></div>
                <div class="mar-top-btm">
                    <h3>ALIVIA A DOR MUSCULAR</h3>
                    <p>Graças a sua tecnologia PodAir, com o acolchoamento macio, você tem absorção
                        de impacto e apoio a cada passo, o que exerce menos pressão nos seus músculos
                        para te proporcionar uma experiência mais confortável de caminhar e ficar de pé
                    </p>
                </div>
            </div>

        </div>
    </section>
    <section id="beneficios">
        <div>
            <h1 class="blue-text porque">MAIS BENEFÍCIOS <span class="pink-text">PARA VOCÊ</span></h1>
        </div>

        <div class=wrapper>

        <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(32, 'full')[0]; ?>'></div>
                <div>
                    <h3>ECONOMIZE</h3>
                    <p>CHEGA DE ÓRTESES CUSTOMIZADAS CARAS OU CIRURGIAS ARRISCADAS
                    </p>
                </div>
            </div>

            <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(31, 'full')[0]; ?>'></div>
                <div>
                    <h3>CUSTOMIZÁVEL</h3>
                    <p>SE ENCAIXA EM TODOS OS SAPATOS E LARGURAS</p>
                </div>
            </div>

            <div class="column">
                <div><img src='<?php echo wp_get_attachment_image_src(30, 'full')[0]; ?>'></div>
                <div>
                    <h3>SUPER LEVE E CONFORTÁVEL</h3>
                </div>
            </div>

            

        </div>
    </section>

    <div style="display: grid;">
        <a href="#" class="comprar">Comprar</a>
    </div>

    <div><h1 class="h-txt-font">LEIA O QUE OS NOSSOS
        CONSUMIDORES TEM A DIZER</h2></div>

    <div class="wrapper">

        <div>
            <img class="photo-buyer" src="<?php echo wp_get_attachment_image_src(39, 'large')[0]; ?>" alt="photo-buyer-1">
            <h3>Joao C</h3>
            <p style="font-size: 1.68rem;">Essas Palmilhas SuperSola são a melhor coisa que eu usei para tratar a minha fascite plantar. E por um bom preço também !</p>
            <img src="<?php echo wp_get_attachment_image_src(34, 'full')[0]; ?>" alt="" class="img_wup">
        </div>


        <div>
        <img class="photo-buyer" src="<?php echo wp_get_attachment_image_src(40, 'large')[0]; ?>" alt="photo-buyer-2">
            <h3>Angela M</h3>
            <p style="font-size: 1.68rem;">Como enfermeira, estou sempre de pé e desde que comecei a usar meus SuperSola, minha terrível dor no calcanhar desapareceu.</p>
            <img src="<?php echo wp_get_attachment_image_src(35, 'full')[0]; ?>" alt="" class="img_wup">
        </div>


    </div>

</div>

<div style=" width: 100%; background: url(<?php echo get_site_url();  ?>/wp-content/uploads/2019/06/certificate_img-min.png);">
    <div class="gar-sat">
        <img src='<?php echo wp_get_attachment_image_src(5, 'full')[0]; ?>' alt="">
        <br>
        <br>
        <p>Reembolso garantido em até 30 dias sem questionamento. Se você está infeliz 
            por qualquer razão, pegue o seu dinheiro de volta. 100% garantido.
        </p>
    </div>
</div>

<div class="container text-center" style="max-width: 80%;">
    <section id="testemunhos">

        <h1 class="blue-text" style="    font-size: 3.2rem; font-weight: 400; margin: 2.52rem 0 0.912rem 0px;">
        USE <span class="pink-text">EM QUALQUER LUGAR:</span>
        </h1>

        <div class="wrapper just">

            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(29, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3 class="pink-text use-txt">SUPERFÍCIES <br>MACIAS</h3>
                </div>
            </div>

            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(27, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3 class="pink-text use-txt">SUPERFÍCIES <br>DURAS</h3>
                </div>
            </div>

            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(28, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3 class="pink-text use-txt">TERRENO <br>ROCHOSO</h3>
                </div>
            </div>

        </div>


        <div>
            <p style="font-size: 1.5rem;">TERRENO ROCHOSO O SuperSola é fabricado com um material leve e respirável, contém
                um gel de silicone que alivia o seu calcanhar e seu pé em áreas críticas que a 
                maioria das palmilhas de sapatos ignora. Pode ser usado todos os dias,e não importa
                quais ativiades você esta pratic​ando, ou o quão duro é o chão, você vai se sentir como
                se estivesse andando nas nuvens!
            </p>
        </div>


        <div class="wrapper just">
            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(16, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3 class="pink-text">CAMINHANDO</h3>
                </div>
            </div>

            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(14, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3>EM PÉ</h3>
                </div>
            </div>

            <div>
                <div>
                    <img src='<?php echo wp_get_attachment_image_src(15, 'full')[0]; ?>' alt="">
                </div>
                <div>
                    <h3 class="pink-text">PRATICANDO <br>ESPORTES</h3>
                </div>
            </div>

        </div>

        <h1 class="blue-text h-txt-font">LEIA O QUE OS NOSSOS CONSUMIDORES <br>TEM A DIZER</h1>

        <div class="wrapper-c">

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(9, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">Érica</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Eles têm um design ótimo e resultados ainda melhores.
                    </p>
                    <br>
                    <p class="testemoni-name">Júlia</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Simplesmente demais!
                    </p>
                </div>
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(10, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">André</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Eu comecei a usar essas palmilhas há cerca de quatro meses. A primeira coisa que notei foi que não sentia mais dores quando ficava em pé.
                    </p>
                    <br>
                    <p class="testemoni-name">Joana</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Eu uso todos os dias, e eles duram. Não mostram nenhum sinal de que precisam ser substituídos.
                    </p>
                </div>
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(11, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">Diego</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Produto excelente e muito prático. Adoro usá-los durante minhas caminhadas.</p>
                    
                </div>
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(12, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">Cinthia</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Top!
                    </p>
                    <br>
                    <p class="testemoni-name">Roberta</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">É ótimo! Muito melhor do que eu imaginava.</p>
                    <br>
                    <p class="testemoni-name">Rafaela</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Não sinto mais dor nem desconforto durante minhas caminhadas... Muito bom!

                    </p>
                </div>
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(7, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">João C.</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Com certeza as melhores palmilhas para fascite plantar.
                    </p>
                    <br>
                    <p class="testemoni-name">Emanuelle</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Muito melhor do que eu imaginava!
                    </p>
                </div>
                <div class="item">
                    <img src="<?php echo wp_get_attachment_image_src(8, 'full')[0]; ?>" alt="">
                    <p class="testemoni-name">Jeferson</p>
                    <div>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                        <i class="material-icons">star</i>
                    </div>
                    <p class="testemoni-text">Mesmo em terrenos íngremes e esburacados não causam nenhum tipo de desconforto.
                        Meu único desejo é que eu tivesse conhecido estas palmilhas antes!
                    </p>
                </div>
            </div>

        </div>

    </section>
</div>

<div class="divider"></div>
<br>
<br>

<section class="text-center shop-wrap" >
<h1 class="blue-text h-txt-font" style="margin-bottom: 50px;">
    <span class="pink-text">Escolha o seu pacote e</span><br> 
ECONOMIZE ATÉ 59%!
</h1>
<div class="shop">
    <div class="col s12 push-m1 m10 l4 sh-pr">
        <div class="product">
            <div class="title_box pink-text"><b>Compre 1 Par</b></div>
            <div><img src="<?php echo wp_get_attachment_image_src(36, 'full')[0]; ?>" alt=""></div>
            <div>
                <div class="crossed-price"><p>DE R$129,00</p></div>
                <div class="full-price pink-text"><p>POR R$119,00</p></div>
                <div class="parcel-pr"><p>6 X R$21,24</p></div>
            </div>
            
            <button class="comprar comprar-btn" id="comp-btn-one">Comprar</button>
            <div id="hid-one" class="text-center">
                <p class="tamanho">Escolha o tamanho do 1°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            </div>


            <div class="btm-txt">
            <p style="margin-bottom: 0px;">&nbsp;</p>
                <p style="margin-bottom: 0px;">+ R$ 14.95 de Frete para todo o Brasil</p>
            </div>
        </div>
    </div>
    <div class="col s12 push-m1 m10 l4 sh-pr">
        <div class="product">
            <div class="title_box pink-text" id='three_products'><b>Compre 2 e Leve 3 Pares</b></div>
            <div><img src="<?php echo wp_get_attachment_image_src(37, 'full')[0]; ?>" alt=""></div>
            <div>
                <div class="crossed-price"><p>DE R$299,00</p></div>
                <div class="full-price pink-text"><p>POR R$199,00</p></div>
                <div class="parcel-pr"><p>6 X R$35,52</p></div>
            </div>
            
            <button class="comprar comprar-btn" id="comp-btn-two">Comprar</button>
            <div id="hid-two" class="text-center">
                <p class="tamanho">Escolha o tamanho do 1°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            

            
                <p class="tamanho">Escolha o tamanho do 2°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            

            
                <p class="tamanho">Escolha o tamanho do 3°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            </div>




            <div class="btm-txt">
                <p style="margin-bottom: 0px;">ECONOMIZE 33%<br>
                FRETE GRÁTIS PARA TODO O BRASIL</p>
            </div>
        </div>
    </div>
    <div class="col s12 push-m1 m10 l4 sh-pr">
    <div class="product">
            <div class="title_box pink-text"><b>Compre 3 e Leve 5 Pares</b></div>
            <div><img src="<?php echo wp_get_attachment_image_src(38, 'full')[0]; ?>" alt=""></div>
            <div>
                <div class="crossed-price"><p>DE R$599,00</p></div>
                <div class="full-price pink-text"><p>POR R$299,00</p></div>
                <div class="parcel-pr"><p>6 X R$53,36</p></div>
            </div>
            
            <button class="comprar comprar-btn" id="comp-btn-three">Comprar</button>
            <div id="hid-three" class="text-center">
                <p class="tamanho">Escolha o tamanho do 1°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            

            
                <p class="tamanho">Escolha o tamanho do 2°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            

            
                <p class="tamanho">Escolha o tamanho do 3°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>

                <p class="tamanho">Escolha o tamanho do 4°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>

                <p class="tamanho">Escolha o tamanho do 5°</p>
                <div class="hom-mul">
                    <div>
                        <button class='variation_btn'>HOMEM</button>
                    </div>
                    <div>
                        <button class='variation_btn'>MULHER</button>
                    </div>
                </div>
            </div>


            
            <div class="btm-txt">
                <p style="margin-bottom: 0px;">ECONOMIZE 50%<br>
                FRETE GRÁTIS PARA TODO O BRASIL</p>
            </div>
        </div>
    </div>

</div>
</section>


  


<div class="text-center">
    <h4>PROMOÇÃO POR TEMPO LIMITADO!</h4>
    <div class="countdown center">
        <p class="timer">
            <span class="timer-horas"></span>:<span class="timer-minutos"></span>:<span class="timer-segundos"></span>
        </p>
            </div>
            
</div>

<br>
<br>

<!-- Green line break -->
<div style="height: 10px;background: #89c419;"></div>

<div style="background:#e7e7e7;">
    <div class="container">
        <div class="row">
            <div class="text-center col s12" style="background:#e7e7e7; min-height:200px;">
                <p style="font-size: 260%; font-weight: 700; margin: 40px 0 10px;"> Desafio 30 dias <span class="pink-text">SuperSola</span><p>
                <p style="font-size: 125%;">GARANTIMOS OS RESULTADOS DO <span class="pink-text">SuperSola</span>. PROVA DISSO,
                    QUE DEVOLVEMOS O SEU DINHEIRO SE O PRODUTO NÃO ATENDER
                    AS EXPECTATIVAS. VOCÊ TERÁ ATÉ 30 DIAS APÓS A COMPRA PARA DEVOLUÇÃO.
                </p>
                <p style="font-size: 150%;font-weight: 600;">SEU RISCO POR TENTAR É ZERO!</p>
                <div>
                    <img src="<?php echo wp_get_attachment_image_src(41, 'medium')[0]; ?>" alt="" style="max-width: 250px; margin-bottom: 40px;">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Green line break -->
<div style="height: 10px;background: #89c419;"></div> 


<div style="color: #e91e63; text-align: center; font-size: 200%; font-weight: 900; padding: 20px 0">Dúvidas Frequentes</div>

<!-- Green line break -->
<div style="height: 10px;background: #89c419;"></div>

<div class="container">
    <div class="row" >
        <div class="col-12" style="padding: 0 10%;">
            <p class="faq-head">QUAL TAMANHO EU DEVO PEDIR?</p>

            <p style="font-size: 120%;">Estes são os tamanhos: 35-39 para Mulheres 40-44
                 para Homens As palmilhas são totalmente customizadas,
                  então você pode cortar las para um ajuste perfeito.
                </p>
            <p class="faq-head">QUAL É A SUA POLÍTICA DE DEVOLUÇÃO?</p>

            <p style="font-size: 120%;">Nós oferecemos uma garantia de reembolso de 30 dias,
                 sem questionamento. Nós queremos os clientes felizes!
                  Se você não estiver satisfeito por qualquer motivo dentro
                   de 30 dias, é só contatar o nosso centro de atenção ao consumidos
                    para um reembolso do seu pedido inicial. Boas Compras!
                </p>
            <p class="faq-head">QUANTOS VOCÊS TÊM EM ESTOQUE?</p>

            <p style="font-size: 120%;">A demanda por este produto tem sido 
                muito alta e nós não estamos conseguindo manter
                 um estoque constante. Se você planeja obter um par
                  de SuperSola, nós recomendamos que você o faça o mais rápido possível.
                </p>

            <p class="faq-head">EM QUANTO TEMPO EU RECEBEREI MEUS SuperSola?</p>
            <p style="font-size: 120%;">Seus SuperSola chegarão entre 3 e 5 dias.</p>

            <p class="faq-head">COMO POSSO ENTRAR EM CONTATO COM VOCÊS?</p>

            <p style="font-size: 120%; margin-bottom:40px;">Você pode enviar um email para sac@supersola.com.br,
                 e nos vamos responder em até um dia util.
                </p>
        </div>

    </div>

</div>

<!-- Green line break -->
<div style="height: 10px;background: #89c419; margin-bottom:30px;"></div>


<div class="container text-center">
     <div class="row">
         <div class="col-12">
             <img src="<?php echo wp_get_attachment_image_src(33, 'large')[0]; ?>" alt="" style="width:100%; height:auto; max-width: 630px;">
             <p "font-size: 120%;">A venda do SuperSola só pode ser realizada através deste SITE OFICIAL.</p>
             <p "font-size: 120%;">Evite falsificações e riscos. NÃO adquira produtos sem procedência e qualidade garantidas.
                  Não nos responsabilizamos por compras realizadas em outros sites. ESTÁ TERMINANTEMENTE PROIBIDA a venda do SuperSola no Mercado Livre, Americanas.com, Submarino, Shoptime entre outros.</p>
         </div>
     </div>


</div>



<!-- Green line break -->
<div style="height: 10px;background: #89c419; margin-top:30px;"></div>








<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content" style="width: 677px; height: 537px; background: url(<?php echo wp_get_attachment_image_src(17, 'full')[0]; ?>) no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Antes de prosseguir precisamos verificar se as entregas de SuperSola
             estão disponíveis para o seu endereço. Por favor preencha o seu Nome e CEP abaixo:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id='ToCheckout'> 
            <div class="modal-wrap">
                <div class="mod-in">
                    <input name="name" id="name" type="text" placeholder="Nome...">
                </div>
                <div class="mod-in">
                    <input name="cep" id="cep" type="text" placeholder="CEP">
                </div>
                <div>
                     <button type="submit" class="mod-btn">Verificar Endereço</button>
                </div>
                <div class="text-center">
                    Seus dados estão completamente protegidos
                </div>
                
            </div>
        
        </form> 
        
      </div>

    </div>
  </div>
</div>


<div>
    <p class="text-center"><b>© 2019 SuperSola</b></p>
</div>

<div class="text-center">
    <p class="foot-nav">
        <a href="<?php echo get_site_url() . '/termos-e-condicoes/'; ?>">TERMOS E CONDIÇÕES</a> | 
        <a href="<?php echo get_site_url() . '/politica-de-privacidade/'; ?>">POLÍTICA DE PRIVACIDADE</a> |
        <a href="<?php echo get_site_url() . '/atendimento-ao-cliente/'; ?>">ATENDIMENTO AO CLIENTE</a>
    </p>
</div>


<script>

var upgradeTime = 18000;
        var seconds = upgradeTime;
        function timer() {
        var days        = Math.floor(seconds/24/60/60);
        var hoursLeft   = Math.floor((seconds) - (days*86400));
        var hours       = Math.floor(hoursLeft/3600);
        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
        var minutes     = Math.floor(minutesLeft/60);
        var remainingSeconds = seconds % 60;
        function pad(n) {
            return (n < 10 ? "0" + n : n);
        }
        jQuery(".timer-horas").html(pad(hours));
        jQuery(".timer-minutos").html(pad(minutes));
        jQuery(".timer-segundos").html(pad(remainingSeconds));
        // document.getElementById('horas').innerHTML =  pad(hours);
        // document.getElementById('minutos').innerHTML = pad(minutes);
        // document.getElementById('segundos').innerHTML = pad(remainingSeconds);
        if (seconds == 0) {
            clearInterval(countdownTimer);
            
        } else {
            seconds--;
        }
        }
    var countdownTimer = setInterval('timer()', 1000);

</script>






<?php get_footer(); ?>