<?php get_header(); ?>


<?php 

    while ( have_posts() ): the_post();
        the_title("<h1 style='margin: 3rem;'>", "</h1>");
        echo "<div style='margin: 3rem;'>";
        the_content();
        echo "</div>";
    endwhile;

?>


<?php get_footer(); ?>