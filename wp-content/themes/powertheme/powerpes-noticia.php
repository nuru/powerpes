<?php
/**
* Template Name: SuperSola Noticia Template
*/
?>


<?php get_header(); ?>
<script src="https://www.youtube.com/iframe_api"></script>

<!-- Video Player styles -->
<style>

.video-overlay{
  width: 100%;
    height: 100%;
    position: absolute;
    background: radial-gradient(circle,rgba(0,0,0,.1),rgba(0,0,0,.8));
    background: -webkit-radial-gradient(circle,rgba(0,0,0,.1),rgba(0,0,0,.8));
    z-index: 2!important;
    top: 0;
    left: 0;
}
.video-cover{
  width:100%;
  height:100%;
  position:absolute;
  top:0;
  left:0;
  z-index:1!important;
}
.play-button-bg{
  position: absolute;
    display: block;
    width: 130px;
    height: 80px;
    border-radius: 5px;
    transition: all .2s ease-out;
    cursor: pointer;
    text-indent: .25em;
  top: 50%;
    left: 50%;
    -webkit-transform: translateY(-50%) translateX(-50%);
    transform: translateY(-50%) translateX(-50%);
    -ms-transform: translate(-50%,-50%);
  background: #da1921;
    -webkit-transition: all .2s ease-out;
}
.play-button-bg:hover{
  -webkit-transform: translateY(-50%) translateX(-50%) scale(1.05);
    transform: translateY(-50%) translateX(-50%) scale(1.05);
    -ms-transform: translate(-50%,-50%) scale(1.05);
    transition: all .2s ease-out;
}
.play-button-graphic{
  display: block;
    width: 130px;
    height: 80px;
    top: 50%;
    left: 50%;
    -webkit-transition: all .2s ease-out;
    transition: all .2s ease-out;
    cursor: pointer;
    position: relative;
    -webkit-transform: translateY(-50%) translateX(-50%);
    transform: translateY(-50%) translateX(-50%);
    -ms-transform: translate(-50%,-50%);
}
.play-button-graphic:before{
  content: "";
    position: absolute;
    border: 24px solid transparent;
    top: 50%;
    border-top-width: 18px;
    border-bottom-width: 18px;
    border-left-color: #fff;
    z-index: 2003;
    left: 50%;
    -webkit-transform: translateY(-50%) translateX(-18%);
    transform: translateY(-50%) translateX(-18%);
    -ms-transform: translate(-18%,-50%);
}
.coverTap{
  /* background-image: url(images/as-seen-on.jpg); */
    background-size: contain;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
}
#bottombutton {
    display: block;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow:    0 8px 0 #fea200, 0 15px 20px rgba(0, 0, 0, .35);
    -moz-box-shadow: 0 8px 0 #fea200, 0 15px 20px rgba(0, 0, 0, .35);
    box-shadow: 0 8px 0 #fea200, 0 15px 20px rgba(0, 0, 0, .35);
    -webkit-transition: -webkit-box-shadow .1s ease-in-out;
    -moz-transition: -moz-box-shadow .1s ease-in-out;
    -o-transition: -o-box-shadow .1s ease-in-out;
    transition: box-shadow .1s ease-in-out;
    font-size: 40px;
    color: #0058ff;
  font-weight:bold;
  margin-bottom:10px;
  text-decoration:none;
}
#bottombutton span {
    display: block;
    padding: 20px 30px;
    background-color: #fcc819;
    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(hsla(42, 85%, 79%, .8)), to(hsla(28, 68%, 73%, .2)));
    background-image: -webkit-linear-gradient(hsla(42, 85%, 79%, .8), hsla(28, 68%, 73%, .2));
    background-image: -moz-linear-gradient(hsla(42, 85%, 79%, .8), hsla(28, 68%, 73%, .2));
    background-image: -o-linear-gradient(hsla(42, 85%, 79%, .8), hsla(28, 68%, 73%, .2));
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
    -moz-box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
    box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
    font-family: Arial, sans-serif;
    line-height: 1;
    text-shadow: 0 -1px 1px rgba(214, 127, 19, .7);
    -webkit-transition: background-color .2s ease-in-out, -webkit-transform .1s ease-in-out;
    -moz-transition: background-color .2s ease-in-out, -moz-transform .1s ease-in-out;
    -o-transition: background-color .2s ease-in-out, -o-transform .1s ease-in-out;
    transition: background-color .2s ease-in-out, transform .1s ease-in-out;
}

#bottombutton:hover span {
    background-color: #ffd202;
    text-shadow: 0 -1px 1px rgba(175, 49, 95, .9), 0 0 5px rgba(255, 255, 255, .8);
}

#bottombutton:active, #button:focus {
    -webkit-box-shadow:    0 8px 0 #fea200, 0 12px 10px rgba(0, 0, 0, .3);
    -moz-box-shadow: 0 8px 0 #fea200, 0 12px 10px rgba(0, 0, 0, .3);
    box-shadow:    0 8px 0 #fea200, 0 12px 10px rgba(0, 0, 0, .3);
}

#bottombutton:active span {
    -webkit-transform: translate(0, 4px);
    -moz-transform: translate(0, 4px);
    -o-transform: translate(0, 4px);
    transform: translate(0, 4px);
}
.linkBlock{padding:12px 35px;border-radius:12px;background:#f4f4f4;display:inline-block;}
#disclaimer .main > div {
    box-shadow: none !important;
    width: 100%;
    line-height: inherit !important;
    background: transparent !important;
    text-align: justify !important;
}

</style>

<nav class="border-dark border-bottom" >
    <div class="my-2 container">
      <div class="row mx-5">
        <img class="logo-header-img" src="<?php echo wp_get_attachment_image_src(51, 'full')[0]; ?>" alt="" style='object-fit: cover; min-width: 365px; max-height: 40px; object-position: -105px'>
        <div class="h6 text-secondary p-0 mt-2 pt-2 ml-auto">sac@supersola.com.br</div>
      </div>
    </div>
  </nav>
  
  <main class="mt-4" >
    <div class="container">
      <div class="row px-2 px-md-5">
        <div class="col-12 col-lg-8">
          <h1 class='h3'>
            Diga Adeus aos Tratamentos Caros e Ineficientes de Uma Vez Por Todas E Recupere seu Bem-Estar!!!
          </h1>
          <div id="video">
              <div>
                  <div class="embed-responsive embed-responsive-16by9">
                    <div class="video-overlay" style="display: block;">
                      <div class="coverTap">
                        <div class="play-button-bg">
                          <div class="play-button-graphic"></div>
                        </div>
                      </div>
                    </div>
                    
                    <img src="<?php echo wp_get_attachment_image_src(215, 'full')[0]; ?>" class='video-cover'>

                    <div id="video-placeholder"></div>

                    <!-- <iframe class="youtube-video" width="560" height="315" src="https://www.youtube.com/embed/mp0nSPMW-RY?enablejsapi=1&version=3&playerapiid=ytplayer&controls=0&playsinline=1&modestbranding=1&fs=0&rel=0&showinfo=0&autoplay=0" frameborder="0" allowfullscreen></iframe> -->
                  </div>
                </div>
                <!-- / Video Embed Code -->
                <script>

                  var player;

                  function onYouTubeIframeAPIReady() {
                    player = new YT.Player('video-placeholder', {
                      width: 600,
                      height: 400,
                      videoId: 'TxlBeStvcrc',
                      playerVars: {
                        color: 'white',
                        origin: '<?php echo json_encode(get_site_url()); ?>',
                        controls: 0,
                        modestbranding: 1,			
                        fs: 0,
                        rel: 0,
                        showinfo: 0,
                        autoplay: 0,
                        iv_load_policy: 3,
                        playsinline: 1,
                      },
                      events: {
                        onReady: initialize
                      }
                    });
                  }

                  function initialize(){

                    // Update the controls on load
                    updateTimerDisplay();
                    // updateProgressBar();

                    // Clear any old interval.
                    if(typeof time_update_interval !== 'undefined') {
                      clearInterval(time_update_interval);
                    }
                    

                    // Start interval to update elapsed time display and
                    // the elapsed part of the progress bar every second.
                    time_update_interval = setInterval(function () {
                      updateTimerDisplay();
                      // updateProgressBar();
                    }, 1000)

                  }

                  // This function is called by initialize()
                  function updateTimerDisplay(){
                    
                    //hide button when 10 minutes is reached
                    if(formatTime( player.getCurrentTime() ) == "10:00") {
                      jQuery("#proximo-passo").show();
                    }

                    // Update current time text display.
                    jQuery('#current-time').text(formatTime( player.getCurrentTime() ));
                    jQuery('#duration').text(formatTime( player.getDuration() ));
                  }

                  function formatTime(time){
                    time = Math.round(time);

                    var minutes = Math.floor(time / 60),
                    seconds = time - minutes * 60;

                    seconds = seconds < 10 ? '0' + seconds : seconds;

                    return minutes + ":" + seconds;
                  }


                  jQuery(function()
                  {

                    //check if user is returning to the site
                    if(localStorage.getItem('return') != null) {
                      jQuery("#proximo-passo").show();
                    }

                    //set local storage when users enters the site for the first time
                    localStorage.setItem("return", "true");

                    /*setTimeout(function() {
                      if(typeof player !== 'undefined') {
                        jQuery('.video-overlay').hide();
                        player.playVideo();
                      }					
                    }, 1000); */

                    /* Play button event */
                    jQuery(".coverTap").on('click', function(e){
                      e.preventDefault();
                      jQuery('.video-overlay').hide();
                      player.playVideo();
                      // jQuery('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');

                      
                    });

                    /* Pause button event */
                    jQuery(".video-cover").on('click', function(e){
                      e.preventDefault();					
                      jQuery('.video-overlay').show();
                      player.pauseVideo();
                      // jQuery('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');

                    });

                  });
                </script>						
              </div>
          <p class='lead'>
          <b><br/>
                Se você sente dores na sola dos pés, nas costas, joelhos e está cansado de gastar dinheiro em fisioterapias, órteses caras, ou usar medicamentos que só resolvem os sintomas temporariamente e ainda podem causar efeitos colaterais, experimente usar SuperSola.
                <br/>
                Com o apoio adequado do arco dos pés e do calcanhar, as palmilhas SuperSola aliviam e protegem o seu calcanhar do desconforto e da irritação, resultando no alívio das dores.
                            <br/>    <br/> 
                A melhor parte é que você não precisa investir em calçados caros, ou palmilhas que podem te custar mais que R$500 e te deixar quebrado!
                <br/>
                Com poucos dias de uso, as palmilhas ajudarão a aliviar desconfortos e dores constantes.

          </b>
          </p>
          <img class="img-look" src="<?php echo wp_get_attachment_image_src(49, 'full')[0]; ?>" alt="">
          <p class="h3 my-4">
                O segredo está na tecnologia PodAir® e no suporte de arco superior.  </p>
          <p class="lead"><b>
                Basta deslizar cada palmilha para dentro do seu tênis favorito para começar a aliviar as suas dores nas costas, e nos pés para que você possa curtir as suas atividades físicas novamente.            <br/>
            <br/>
                O SuperSola é fabricado com um material leve e respirável, contém um gel de silicone com uma tecnologia patenteada (PodAir®) que alivia o seu calcanhar e seu pé em áreas críticas que a maioria das palmilhas de sapatos ignora.
            <br/>
            <br/>
                Pode ser usado todos os dias, e não importa quais atividades você esta praticando, ou o quão duro é o chão, você vai se sentir como se estivesse andando nas nuvens!
          </b></p>
          <p class="h5">SuperSola Estimula O Bem Estar do Corpo Assim:</p>
          <ul class='ml-5 pl-2' style="list-style-image: url(<?php echo get_site_url();  ?>/wp-content/uploads/2019/06/angel-min.png)" >
            <li class='lead'><b>Reduzindo Dores nas Costas</b></li>
            <li class='lead'><b>ALIVIA A DOR NAS ARTICULAÇÕES</b></li>
            <li class='lead'><b>ALIVIA A DOR MUSCULAR</b></li>
            <li class='lead'><b>Melhorando a Circulação Sanguínea</b></li>
          </ul>
          <p class="lead"><b>
            Chega de ficar com dores que não permitem que você curta as atividades.          </b></p>
          <p class="h3 my-4">
            Você Merece Viver uma Vida Livre de Dor

          </p>
          <p class="lead"><b>
            Chega de consultas ao médico, fisioterapeutas ou órteses caras!             <br/>
             SuperSola alivia o seus pés arqueados e estimula os pés para trazer equilíbrio, harmonia e redução da dor em todo o seu corpo.          </b></p>
          <p class="h5">
       
          <p class="h3 my-5">
            Milhares de brasileiros estão trocando seus tratamentos caros por SuperSola           </p>
          <p class="h5 my-4">
            Centenas de clientes satisfeitos estão falando a respeito desta invenção revolucionária que está tornando a sua vida muito mais fácil!

          </p>
          
          <div class="my-3 d-flex align-items-center">
            <img src="<?php echo wp_get_attachment_image_src(48, 'full')[0]; ?>" alt="" class="rounded-circle">
            <p class='ml-4' >
              <span class="h5">Amanda S. —</span>
              <span class="lead"><b>“Eu achava que elas iriam funcionar, mas depois de 2 semanas minhas costas estão como se eu tivesse 20 anos novamente. Eu amei”. </b></span>
            </p>
          </div>

          <div class="my-3 d-flex align-items-center">
            <img src="<?php echo wp_get_attachment_image_src(50, 'full')[0]; ?>" alt="" class="rounded-circle">
            <p class='ml-4' >
              <span class="h5">Márcia P — </span>
              <span class="lead"><b>“Depois que li o artigo eu estava muito cética, eu não acreditava que elas poderiam realmente fazer alguma diferença. Mas já faz um mês desde que comecei a usá-las e devo dizer que agora acredito em cada palavra. Hoje eu não sinto mais aquela terrível dor nos pés no final dia.”</b></span>
            </p>
          </div>
          
          <div class="my-3 d-flex align-items-center">
            <img src="<?php echo wp_get_attachment_image_src(57, 'full')[0]; ?>" alt="" class="rounded-circle">
            <p class='ml-4' >
              <span class="h5">João C. — </span>
              <span class="lead"><b>“Minha fisioterapia custa muito caro para minhas costas. Eu não conseguia mais pagá-la. Meu amigo me pediu para testar e eu não tenho mais ido às sessões de fisioterapia.”</b></span>
            </p>
          </div>
                    
          <div class="my-3 d-flex align-items-center">
            <img src="<?php echo wp_get_attachment_image_src(59, 'full')[0]; ?>" alt="" class="rounded-circle">
            <p class='ml-4' >
              <span class="h5">Carlos R. — </span>
              <span class="lead"><b>"Este foi o melhor investimento que eu já fiz. Eu estava gastando muito dinheiro com Fisioterapia toda semana. E já não preciso ir desde quando coloquei essas palmilhas nos meus sapatos.” </b></span>
            </p>
          </div>
          
          <p class="h2 my-4">
            Aonde Eu Posso Conseguir uma Desta?
          </p>
          
          <p style="font-size: 30px" class="lead"><b>
            Devido a uma demanda excessiva, está difícil manter o estoque em dia. Por isso hoje há um <a href="#">estoque limitado</a> de SuperSola. Clique no Botão Laranja abaixo para descobrir se elas ainda estão disponíveis. 
          </b></p>
          
          <a href="<?php echo get_site_url();  ?>" class="super-btn-2 py-3 w-100 text-center d-block m-auto">
            <p class="h3 m-0 p-0">
              <b>Confira a Disponibilidade Atual</b>
              <img class="d-inline" src="<?php echo wp_get_attachment_image_src(52, 'thumbnail')[0]; ?>" alt="">
            </p>
          </a>
          
          <div class="my-5"></div>
          
        </div>
        
        
        
        
        <div class="col-12 col-lg-4 text-center">
          <img src="<?php echo wp_get_attachment_image_src(56, 'medium')[0]; ?>" alt="">
          <h2 class='h3' >Palmilhas SuperSola</h2>
          <img class='d-block m-auto' width='140' src="<?php echo wp_get_attachment_image_src(53, 'medium')[0]; ?>" alt="">
          <div class="h5">PRODUTO 5 ESTRELAS</div>
          <img src="<?php echo wp_get_attachment_image_src(58, 'full')[0]; ?>" alt="" style="width:300px; height:300px;">
          <a href="<?php echo get_site_url();  ?>" class="h6 w-100 justify-content-between align-items-center py-3 my-4 d-flex super-btn">
            <b> Confira a Disponibilidade Atual</b>
            <img width="30" class="d-inline-block" src="<?php echo wp_get_attachment_image_src(52, 'thumbnail')[0]; ?>" alt="">
          </a>
        </div>
      </div>
    </div>
  </main>
  
<div style="background-color: #000" class="text-center py-3">
    <div class="my-4 h6 text-white">
      Copyright 2018 sac@supersola.com.br | 880 United States
    </div>
    <div class="my-4 text-white foot-link">
      <a href="<?php echo get_site_url(); ?>/termos-e-condicoes/">Termos de Uso</a>
      <span class="mx-4">|</span>
      <a href="<?php echo get_site_url(); ?>/politica-de-privacidade/">Política de privacidade</a>
      <span class="mx-4">|</span>
      <a href="<?php echo get_site_url(); ?>/atendimento-ao-cliente/">Contato</a>
    </div>
</div>


<?php get_footer(); ?>