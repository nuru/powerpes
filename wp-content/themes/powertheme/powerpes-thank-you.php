<?php
/**
 * Template Name: Thank you Page
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php 

    global $wpdb;
    $ord_id = (int) $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_type = 'shop_order' ORDER BY post_date DESC LIMIT 1");


    $order = wc_get_order( $ord_id );

?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>
	
	<?php 
		
		$order_details = "<div class='order_details_cls'>";
			
		$get_items = $order->get_items();	
		
		if ( count($get_items) > 0 ) {
			$order->calculate_totals();
		}
		
		$order_details .= "<div class='product_details_cls'>";
		
		// Iterate though each order item
		foreach ($get_items as $item_id => $item) 
		{
		 	$order_details .= "<div class='order_item'>";
		 	// Get the WC_Order_Item_Product object properties in an array
		       $item_data = $item->get_data();
		
		     if ($item['quantity'] > 0) 
		     {
		           // get the WC_Product object
		         $product = wc_get_product($item['product_id']);
		         $product_name = $product->get_name();
		         $product_img = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'medium' )[0];
				
		         $order_details .= "<img src='{$product_img}' class='product_img'> <p class='txt_product'> {$product_name} ";
		         $order_details .= "$";
		         $order_details .= "{$item->get_total()}</p>";
				
		        
		     }
		    
		     $order_details .= "</div>";
		}
		
		$order_details .= "</div>";
		
		$order_data = $order->get_data();
	
		//billing & shipping details
		
		$order_details .= "<div class='billing_and_shipping'>";
		
		$order_details .= "<div class='billing_cls'>";
		
		$order_details .= "<h3>Billing Details</h3>";
		
		$order_details .= "<p><span> First Name: </span>" . $order_data['billing']['first_name'] . "</p>";
		$order_details .= "<p><span> Last Name: </span>" . $order_data['billing']['last_name'] . "</p>";
		$order_details .= "<p><span> Address: </span>" . $order_data['billing']['address_1'] . "</p>";
		$order_details .= "<p><span> City: </span>" . $order_data['billing']['city'] . "</p>";
		$order_details .= "<p><span> State: <span>" . $order_data['billing']['state'] . "</p>";
		$order_details .= "<p><span> Zip: </span>" . $order_data['billing']['postcode'] . "</p>";
		$order_details .= "<p><span> Country: </span>" . $order_data['billing']['country']. "</p>";
		
		$order_details .= "</div>";
		$order_details .= "<div class='shipping_cls'>";
		
		$order_details .= "<h3>Shipping Details</h3>";
		
		$order_details .= "<p>" . $order_data['shipping']['first_name'] . "</p>";
		$order_details .= "<p>" . $order_data['shipping']['last_name'] . "</p>";
		$order_details .= "<p><p><span> Address: </span>" . $order_data['shipping']['address_1'] . "</p>";
		$order_details .= "<p><span> City: </span>" . $order_data['shipping']['city'] . "</p>";
		$order_details .= "<p><span> State: <span>" . $order_data['shipping']['state'] . "</p>";
		$order_details .= "<p><span> Zip: </span>" . $order_data['shipping']['postcode'] . "</p>";
		$order_details .= "<p><span> Country: </span>" . $order_data['shipping']['country']. "</p>";
		
		
		// echo "<pre>";
		// print_r($order_data);
		// echo "</pre>";
	
		$order_details .= "</div>";
		$order_details .= "</div>";
		
		
		echo $order_details;
	
	?>

	<?php else : ?>

		<?php 
			$order_details .= "<p>No order information</p>";
		?>
		
	<?php endif; ?>
    <br> <hr>

    <a href='<?php echo get_site_url(); ?>'>Home</a>

</div>



                   