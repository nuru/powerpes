<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta http-equiv="X-UA-Compatible" content="ie=edge"> -->
    <title>SuperSola</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    

    <?php wp_head(); ?>    

    <?php 
        if(is_front_page()) {
            ?>             

                <script async src="<?php echo get_template_directory_uri() . '/js/owl.carousel.min.js'?>"></script>
                <script async src="<?php echo get_template_directory_uri() . '/js/carousel.js'?>"></script>
                <script async src="<?php echo get_template_directory_uri() . '/js/comprar.js'?>"></script>
            
            <?php
        }
    ?>

</head>
<body>

