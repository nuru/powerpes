    
<?php wp_footer(); ?>


<?php 
    if(is_front_page()) {
        ?>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/owl.carousel.min.css' ?>">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/owl.theme.default.min.css' ?>">
        <?php
    }
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>