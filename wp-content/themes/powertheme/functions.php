<?php


function load_stylesheets()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), false, 'all');
    wp_enqueue_style('style');   

}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function include_jquery()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.4.1.min.js',  '', 1 , false);
    add_action('wp_enqueue_scripts', 'jquery');    
}
add_action('wp_head', 'include_jquery');


add_filter('gettext', 'translate_reply', 999, 3);
add_filter('ngettext', 'translate_reply', 999, 3);
  
//global translations
function translate_reply($translated) {   

	// if($translated == 'Shipping') {        
	// 	return 'Frete';
	// } else if($translated == 'Free!') {
	// 	return 'Grátis';
    // }
    
    // STRING 1
    $translated = str_ireplace( 'Shipping', 'Frete', $translated );
    
    // STRING 2
    $translated = str_ireplace( 'Free!', 'Grátis', $translated );
    $translated = str_ireplace( 'Free', 'Grátis', $translated );


    $translated = str_ireplace( "WHAT'S INCLUDED IN YOUR PLAN?", 'Informação Adicional', $translated );


    $translated = str_ireplace( "Card Holder Name", 'Nome do Titular', $translated );
    $translated = str_ireplace( "Expiry", 'Data de validade', $translated );
    $translated = str_ireplace( "NAME PRINTED ON CARD", 'NOME IMPRESSO NO CARTÃO', $translated );
    $translated = str_ireplace( "Name Printed On Card", 'NOME IMPRESSO NO CARTÃO', $translated );
    $translated = str_ireplace( "CARD OWNER CPF", 'PROPRIETÁRIO DO CARTÃO CPF', $translated );
    $translated = str_ireplace( "(as recorded on the card)", '', $translated );
    $translated = str_ireplace( "as recorded on the card", '', $translated );
    $translated = str_ireplace( "MM/YYYY", 'MM/AAAA', $translated );
    $translated = str_ireplace( "with interest", 'com taxas', $translated );
    $translated = str_ireplace( "Realizar Pagamento", 'Sim! Envie Meu SuperSola Agora! →', $translated );
    $translated = str_ireplace( "Quantidade de parcelas", 'Número de parcelas', $translated );
    $translated = str_ireplace( "Order Received", 'Pedido Recebido', $translated );

    
    return $translated;	

}

function fkwp_enqueue_additional_scripts()
{    
    wp_enqueue_script('extrafuncjs', get_template_directory_uri() . '/js/extra-func.js',  ['jquery'], 1, false);    
}

add_action('wp_head', 'fkwp_enqueue_additional_scripts');

// function fkwp_redirect_order_received() 
// {
//     $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    
//     if(strpos($actual_link, 'order-received')) {
//         wp_redirect(get_site_url() . "/thank-you-page");
//         exit;
//     }
// }

// add_action('init', 'fkwp_redirect_order_received');