<?php
/**
* Template Name: PowerPes 2-0
*/
?>

<?php 
    if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
        define( 'WOOCOMMERCE_CHECKOUT', true );
    }
?>
<?php get_header(); ?>


<div class="text-center">

    <div class="text-center head-f" style=" width: 100%; min-height: 50px; background: url(<?php echo get_site_url();  ?>/wp-content/uploads/2019/06/canvas-min.png);background-color: #dc3545; z-index: 1000;">
        
        <p class="para-head">LIQUIDAÇÃO! Compre Hoje para ter Até 55% OFF</p>
    </div>

    <div class="top-wrap">
        
            <div class="top-head">
                <img src="<?php echo wp_get_attachment_image_src(18, 'medium')[0]; ?>" alt="">
                <img class="bbb" src="<?php echo wp_get_attachment_image_src(68, 'full')[0]; ?>" alt="">
                <img class="ssl-ser" src="<?php echo wp_get_attachment_image_src(66, 'full')[0]; ?>" alt="">
        
            <div class="braz-cont">
                <img src="<?php echo wp_get_attachment_image_src(69, 'full')[0]; ?>" alt="">
                <div class="text-center cont">
                    <b>Contato:</b>
                    <br>
                    sac@euqueroja.com
                </div>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="wrap-dash">

            <div class="row flex-row align-items-center">
                <div class="col-12 col-md-5">
                    <img style="width:100%;" src="<?php echo wp_get_attachment_image_src(58, 'full')[0]; ?>" alt="">    
                </div>
                <div class="col-12 col-md-7">
                    <h2>Alivie as Suas Dores nas Costas, Joelhos e Pés enquanto Anda!</h2>

                    <ul class="list-unstyled text-left list-margin">
                    <li class="d-flex align-items-center">
                    <img class="mr-3 arrowList" src="<?php echo wp_get_attachment_image_src(71, 'full')[0]; ?>" alt="">
                    <p>PowerPés melhora as dores nos pés e nas costas, ajuda na rigidez e dores nas articulações, musculares e muito mais</p>
                    </li>
                    <li class="d-flex align-items-center">
                    <img class="mr-3 arrowList" src="<?php echo wp_get_attachment_image_src(71, 'full')[0]; ?>" alt="">
                    <p>Tudo que você precisa fazer é cortá-las no tamanho desejado, colocar em seus sapatos e essas palmilhas vão fazer a sua mágica</p>
                    </li>
                    <li class="d-flex align-items-center">
                    <img class="mr-3 arrowList" src="<?php echo wp_get_attachment_image_src(71, 'full')[0]; ?>" alt="">
                    <p>SUPER Leve E Confortável — PowerPés se encaixa em todos os sapatos e larguras!</p>
                    </li>
                    <li class="d-flex align-items-center">
                    <img class="mr-3 arrowList" src="<?php echo wp_get_attachment_image_src(71, 'full')[0]; ?>" alt="">
                    <p>Poupe Dinheiro com Massagens, Cirurgias caras, ou Medicamentos Perigosas..</p>
                    </li>
                </ul>
                </div>
            </div>

        


    </div>


        <div style="background-color: rgb(255, 235, 148);" class="text-center border rounded border-warning py-4 mt-3 px-4 d-flex flex-wrap justify-content-center align-items-center">
        <img style="height: 40px;" src="<?php echo wp_get_attachment_image_src(70, 'thumbnail')[0]; ?>" class="d-inline-block" height="40" width="40" alt="">
        <p style="line-height: 1em;" class="h6 m-0 p-0 d-inline">PowerPés está em alta demanda, e o estoque está limitado. Nós já reservamos o seu pedido pelos próximos <span class="timer-fire">0:00</span>  minutos.</p>
        </div>



        <div class="row">

            <div class="col-12 p-3 col-lg-6">
                <div class="card">
                    <div style="width:100%; height:500px; background:pink;">
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>

                <div class="text-center h5 mb-0 pb-0">
                    <p>O Que Os Consumidores estão dizendo sobre PowerPés</p>
                    <hr>
                    <img style="width:100%;" src="<?php echo wp_get_attachment_image_src(74, 'full')[0]; ?>" alt="">
                </div>
                </div>
            </div>

            <div class="col-lg-6 p-3">
                <div class="card" style="width:100%; height:680px;">
                <p class="h5 uppercase pb-0 mb-0 text-center">PASSO 3: OPÇÕES DE PAGAMENTO</p>
                <hr>
                    <div style="width:100%; height:500px; background:violet;">
                        Checkout2
                    </div>

                    <div class="text-center">
                        <img src="<?php echo wp_get_attachment_image_src(67, 'full')[0]; ?>" alt="">
                    </div>
                    <div class="text-center mt-4" style="display:flex;">
                        <img class="d-block mr-4" src="<?php echo wp_get_attachment_image_src(65, 'medium')[0]; ?>" alt="" style="height:205px;">
                        <p><b>GARANTIA DE 60 DIAS:</b> Se você comprar os PowerPés e por algum motivo não ficar satisfeito 
                        – nós te oferecemos uma garantia de 60 dias em todas as compras. Basta enviar-nos de volta o(s) item(s)
                        para receber um reembolso completo ou troca, menos o valor do Frete!</p>
                    </div>
                </div>

            </div>


        </div>

        <div class="card px-5">
                <div class="h4 mt-5 text-center">PERGUNTAS FREQUENTES</div>
                
                
                    <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                    <b>Qual tamanho eu devo pedir?</b>
                    </div>
                    <div class="mb-4 mt-3 ml-3 ml-md-5">
                        Estes&nbsp;são&nbsp;os&nbsp;tamanhos:  35-39&nbsp;para&nbsp;Mulheres 40-44&nbsp;para&nbsp;Homens  As palmilhas são totalmente customizadas, então você pode cortar las para um ajuste perfeito.                 </div>

                
                    <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                    <b>Qual é a sua política de devolução?</b>
                    </div>
                    <div class="mb-4 mt-3 ml-3 ml-md-5">
                        Nós oferecemos uma garantia de reembolso de 60 dias, sem questionamento. Nós queremos os clientes felizes! Se você não estiver satisfeito por qualquer motivo dentro de 60 dias, é só contatar o nosso centro de atenção ao consumidos para um reembolso do seu pedido inicial. Boas Compras!                 </div>

                
                    <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                    <b>Quantos vocês têm em estoque?</b>
                    </div>
                    <div class="mb-4 mt-3 ml-3 ml-md-5">
                        A demanda por este produto tem sido muito alta e nós não estamos conseguindo manter um estoque constante. Se você planeja obter um par de PowerPés, nós recomendamos que você o faça o mais rápido possível.                 </div>

                
                    <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                    <b>Em quanto tempo eu receberei meus PowerPés?</b>
                    </div>
                    <div class="mb-4 mt-3 ml-3 ml-md-5">
                        Seus PowerPés chegarão entre 16 e 21 dias.                 </div>

                
                    <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                    <b>Como posso entrar em contato com vocês? </b>
                    </div>
                    <div class="mb-4 mt-3 ml-3 ml-md-5">
                        Você pode enviar um email para sac@euqueroja.com, e nos vamos responder em até um dia util.                  </div>

                                
            </div>

            <div class="bg-light text-secondary mt-5 pb-5 text-center px-3 foot-link">
    
                    <p class="foot-stuff">
                        <a href="https://doseperfeita.com/Termos.htm">TERMOS E CONDIÇÕES</a> <span class="mx-2">|</span>
                        <a href="https://doseperfeita.com/Pol%C3%ADtica%20de%20Privacidade.htm">POLÍTICA DE PRIVACIDADE</a> <span class="mx-2">|</span> 
                        <a href="https://doseperfeita.com/Contato.htm">CONTATO</a>
                        <br>
                        <br>
                        <span style="font-size: 13px">
                            By filling out the field, you consent for MagniSole to use automated technology, including texts
                            and prerecorded messages, to contact you at the number and email provided about MagniSole offers.
                        </span>
                    </p>
                    
                    <img class="d-block mx-auto mt-3" src="<?php echo wp_get_attachment_image_src(63,'thumbnail')[0];?>" alt="">
    
                </div>









    </div>















</div>


<script>

var upgradeTime = 599;
        var seconds = upgradeTime;
        function timer() {
        var days        = Math.floor(seconds/24/60/60);
        var hoursLeft   = Math.floor((seconds) - (days*86400));
        var hours       = Math.floor(hoursLeft/3600);
        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
        var minutes     = Math.floor(minutesLeft/60);
        var remainingSeconds = seconds % 60;
        function pad(n) {
            return (n < 10 ? "0" + n : n);
        }
       
        jQuery(".timer-fire").html(pad(minutes) + ":" + pad(remainingSeconds) );
        
        if (seconds == 0) {
            clearInterval(countdownTimer);
            
        } else {
            seconds--;
        }
        }
    var countdownTimer = setInterval('timer()', 1000);

</script>

<?php get_footer(); ?>