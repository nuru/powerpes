<?php/* Template Name: PowerPes 2-0 Blank */ ?>

<?php 
	$ver = '1.1';
?>

<head>	
<?php wp_head(); ?>		
	<script async src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<?php
  global $woocommerce;
  $logo = get_field('logo');
  $produtos = get_field('produtos');
  $faq = get_field('faq');
  $questions = $faq['questions'];
  $answers = $faq['answers'];
  $woocommerce->cart->empty_cart();
  $woocommerce->cart->add_to_cart($produtos[0]);
  $products = [];
  $json_variations_list = array();
  foreach($produtos as $key => $product_id){
     $produto = wc_get_product($product_id);
     $products[$key]['produto'] = $produto;
     $variations = $produto->get_available_variations();
	 $products[$key]['productid'] = $product_id;
     $products[$key]['regularprice'] = $variations[0]['display_regular_price'];
     $products[$key]['saleprice'] = $variations[0]['display_price'];
	 $variations_list = array();
     foreach($variations as $variation){
     	$products[$key]['variations'][] = $variation;
		$_name = $variation['attributes']['attribute_tamanhos'];
        $_id = $variation['variation_id'];
		$variations_list[$_name] = $_id; 
     }
	 $json_variations_list[$product_id] = $variations_list;
  }
?>         
 <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

     <style>
	 
	 .payment_method_ebanx-banking-ticket{
		display:none;
	}
	 
	 @keyframes pulse_animation {
    0% { transform: scale(1); }
    30% { transform: scale(1); }
    50% { transform: scale(1.02); }
    70% { transform: scale(1); }
	100% { transform: scale(1); }

}

	.blockOverlay {
	position: relative!important;
	display: none!important;
	} 

	 #place_order{
		 background-color:#1abb3e !important;
		 animation-name: pulse_animation;
    animation-duration: 1000ms;
    transform-origin:70% 70%;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
	transition: background-color 100ms linear;
	 }
	 
	 #place_order:hover{
	background-color:green !important;
	animation-name: pulse_animation;
    animation-duration: 1000ms;
    transform-origin:70% 70%;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
	 }
    img.pinkarrowicon {
        width: 40px;
        position: absolute;
        left: -34px;
        top: calc(50% - 15px);
    }
       
    .spinning{
      animation: spin 12s linear infinite;
    }

    @-moz-keyframes spin {
        from { -moz-transform: rotate(0deg); }
        to { -moz-transform: rotate(-360deg); }
    }
    @-webkit-keyframes spin {
        from { -webkit-transform: rotate(0deg); }
        to { -webkit-transform: rotate(-360deg); }
    }
    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(-360deg);}
    }

    .snow{
      background-image: url(https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/images/canvas.png)
    }

    .hidden{
      display: none !important;
    }

    label.special-radio{
      margin: auto;
    }

    label.special-radio input{
      display: none;
    }

    label.special-radio span{
      display: flex;
      border-radius: 100%;
      margin: auto;
      width: 22px;
      height: 22px;
      border: 2px solid darkblue;
    }
    label.special-radio img{
      width: 12px !important;
      height: 12px !important;
      margin: auto;
      display: none;
    }

    label.special-radio input:checked ~ span{
      background-color: darkblue;
    }
    label.special-radio input:checked ~ span img{
      display: block;
    }
	
	.arrowList{
		width:30px;
		height:30px;
	}
	
	.woocommerce-privacy-policy-text{
		display:none;
	}
    
  </style>
<div class="bg-light p-0 m-0">
<!-- TOP HEADER -->
 <div style="z-index: 1000;" class="snow position-fixed w-100 py-3 text-center bg-danger">
  <img class="d-inline" src="" alt="">
   <div class="h5 m-0 d-inline text-white"><b>LIQUIDAÇÃO! Compre Hoje para ter Até 55% OFF</b></div>
 </div>
 
<!-- LOGO NAV -->
 <div class="py-4 bg-white">
   <div style="padding-top: 80px;" class="w-100 row d-flex flex-wrap justify-content-center align-items-center">
     <img class="d-block m-sm-auto ml-4" width="250" src="<?php echo $logo; ?>" alt="">
     <img class="d-none d-md-block m-sm-auto m-0" src="https://gadgetweb-9bvb8shtos4t2j1exw.stackpathdns.com/magnisolecmsfull/img/bbb.png" alt="">
     <img class="d-none d-md-block m-sm-auto m-0" src="https://gadgetweb-9bvb8shtos4t2j1exw.stackpathdns.com/magnisolecmsfull/img/ssl.png" alt="">
     <div class="d-flex m-sm-auto m-0 align-items-center" >
      <img class="mx-3" src="https://gadgetweb-9bvb8shtos4t2j1exw.stackpathdns.com/magnisolecmsfull/img/BR.png" alt="">
      <div class="d-none d-md-block text-center">
        <b>Contato:</b>
        <br/>
        sac@euqueroja.com
      </div>
     </div>
   </div>
 </div>
 
<!-- MAIN  -->
 <main>
   <div class="container">
<!-- TOP -->
     <div style="border: 2px dashed black;" class="py-2 my-2 bg-white">
       <div class="row flex-row align-items-center">
         <div class="col-12 col-md-5">
           <img width="100%" src="https://euqueroja.com/wp-content/uploads/2019/01/beneficios.gif" alt="">
         </div>
         <div class="col-12 col-md-7 text-center">
           <h1 class="h2 mx-5" >Alivie as Suas Dores nas Costas, Joelhos e Pés enquanto Anda!</h1>
           <ul class="list-unstyled text-left">
            <li class="d-flex align-items-center">
              <img class="mr-3 arrowList" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/list-arrow.svg" alt="">
              <p>PowerPés melhora as dores nos pés e nas costas, ajuda na rigidez e dores nas articulações, musculares e muito mais</p>
            </li>
            <li class="d-flex align-items-center">
              <img class="mr-3 arrowList" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/list-arrow.svg" alt="">
              <p>Tudo que você precisa fazer é cortá-las no tamanho desejado, colocar em seus sapatos e essas palmilhas vão fazer a sua mágica</p>
            </li>
            <li class="d-flex align-items-center">
              <img class="mr-3 arrowList" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/list-arrow.svg" alt="">
              <p>SUPER Leve E Confortável — PowerPés se encaixa em todos os sapatos e larguras!</p>
            </li>
            <li class="d-flex align-items-center">
              <img class="mr-3 arrowList" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/list-arrow.svg" alt="">
              <p>Poupe Dinheiro com Massagens, Cirurgias caras, ou Medicamentos Perigosas..</p>
            </li>
           </ul>
           
         </div>
       </div>
     </div>
     <div style="background-color: rgb(255, 235, 148);" class="text-center border rounded border-warning py-4 mt-3 px-4 d-flex flex-wrap justify-content-center align-items-center">
       <img style="height: 40px;" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/fire.svg" class="d-inline-block" height="40" width="40" alt="">
       <p style="line-height: 1em;" class="h6 m-0 p-0 d-inline">PowerPés está em alta demanda, e o estoque está limitado. Nós já reservamos o seu pedido pelos próximos <span class="countdown">9:59</span>  minutos.</p>
     </div>
<!-- BOTTOM -->
     <div id="form-replacer">
       <div class="row">
         <div class="col-12 p-3 col-lg-6">
           <div class="card p-3">
             <div class="row align-items-center">
               <div class="col-4">
                <div style="width: 100px; height: 100px;" class="position-relative bg-danger rounded-circle d-flex justify-content-center align-items-center">
                 <div style="border: white dashed 2px; height: 90px; width: 90px;" class="spinning rounded-circle d-flex justify-content-center align-items-center" >
                 </div>
                 <div class="h4 w-75 text-white text-center m-0 p-0 position-absolute">55% OFF</div>         
                </div>
               </div>
               <div class="col-8 text-center">
                 <div class="h5 text-danger">
                   O Seu Desconto de 55% Já Foi Aplicado
                 </div>
                 <p>
                   O Seu Pedido Está Qualificado Para  <b>FRETE GRÁTIS</b> Se For Realizado HOJE
                 </p>
               </div>
             </div>
             <p class="h5 uppercase pb-0 mb-0">PASSO 1: SELECIONE A QUANTIDADE DO PEDIDO</p>
             <hr class="mt-2" >
             <div class="px-2" >
              <?php
               foreach($products as $key => $product):
               $produto = $product['produto'];
               $name = $produto->get_name();
               $variations = $product['variations'];
               $productid = $produto->get_id();
               $regularprice = $product['regularprice'];
               $saleprice = $product['saleprice'];
			   $count = explode(" PAR", $name);
			   $count = explode(" ", $count[0]);
			   $count = $count[count($count)-1];
               ?>
               
               <div class="px-3 row py-2 <?php if($key === 0){echo "bg-warning";} ?> rounded position-relative align-items-center">
                 <div class="mx-auto px-0 col-1">
                  <label class="special-radio" >
                    <input class="selectProduct" onchange="unhideOptions(<?php echo $productid; ?>)" type="radio" name="quantity"/>
                    <span><img src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/check.svg" alt=""></span>
                  </label>
                 </div>
                 <div class="mx-auto px-0 col-5">
                   <p>
                     <b><?php if($key === 0){echo "MAIS VENDIDO";} ?></b> <?php echo $name; ?>
                   </p>
                 </div>
                 <div class="mx-auto px-0 col-6 text-right">
                   <b style="text-decoration: line-through;" >$<?php echo number_format($regularprice, 2, ',', ''); ?> preço normal</b>
                   <br/>
                   <b>R$<?php echo number_format($saleprice, 2, ',', ''); ?> preço promocional</b>
                   <?php echo ($count > 1 ? "<b>FRETE GRÁTIS</b>" : "") ?>
                 </div>
                 <?php if($key === 0): ?>
                   <img src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/pink_arrow.svg" class="pinkarrowicon" alt="">
                 <?php endif; ?>
               </div>
               
               <div id="produto<?php echo $productid; ?>" data-productId="<?php echo $productid; ?>" class="row product-variations d-none mb-3">
				 
               <?php for($i = 1; $i <= $count; $i++):?>
			   <div class="col-12 text-center"><b>Escolha o tamanho do <?php echo $i; ?>°</b></div>
			   <div class="size-choicer col-12 row" data-selected="-1">
				<div class="col-12 col-sm-6 mt-2 px-2 text-center" data-size="0"><button type="button" role="button" aria-pressed="true" class="sizeButton w-100 btn btn-secondary">34-39</button></div>
				<div class="col-12 col-sm-6 mt-2 px-2 text-center" data-size="1"><button type="button" role="button" aria-pressed="true" class="sizeButton w-100 btn btn-secondary">40-45</button></div>
			   </div>
               <?php endfor; ?>
               </div>
               
               <?php
               endforeach;
              ?>
              
             </div>
             <p class="h5 uppercase pb-0 mb-0">PASSO 2: INFORMAÇÕES DE ENVIO</p>
             <hr class="mt-2" >
             <div id="client-info-super" action="" class="row">
               <div class="form-group col-12 col-sm-6">
                 <input type="text" placeholder="First name" class="form-control">
               </div>
               <div class="form-group col-12 col-sm-6">
                 <input type="text" placeholder="Last name" class="form-control">
               </div>
               <div class="form-group col-12">
                 <input type="email" placeholder="Email" class="form-control">
               </div>
               <div class="form-group col-12">
                 <input type="cel" placeholder="Phone" class="form-control">
               </div>
             </div>
             <p class="h5 mb-0 pb-0 text-center">O Que Os Consumidores estão dizendo sobre PowerPés</p>
             <hr class="mt-2">
             <img class="w-100 d-block" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/images/reviews.png" alt="">
           </div>
         </div>
         <div class="col-lg-6 p-3">
           <div class="card align-items-start p-3">
             <p class="h5 uppercase pb-0 mb-0">PASSO 3: OPÇÕES DE PAGAMENTO</p>
             <hr class="mx-2 w-100" >
             <div id="payment-super" class="row px-3">

               <div class="form-group m-0 align-items-center d-flex">
                 <label class="ml-0 mr-3 special-radio">
                   <input name="payment" value="paypal_payment" class="mx-3" type="radio">
                   <span><img src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/check.svg" alt=""></span>
                 </label>
                 <img width="120" src="https://gadgetweb-9bvb8shtos4t2j1exw.stackpathdns.com/magnisolecmsfull/img/paypal.png" alt="">
               </div>
               <hr class="mx-2 w-100">
               <div class="form-group m-0 align-items-center d-flex flex-wrap">
                 <label class="ml-0 mr-3 special-radio">
                   <input name="payment" value="credit_payment" class="mx-3" type="radio">
                   <span><img src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/check.svg" alt=""></span>
                 </label>
                 <span class="mr-3 lead"><b>Cartão de Crédito</b></span>
                 <img width="40" class="mx-2" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/visa.svg" alt="">
                 <img width="40" class="mx-2" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/american-express.svg" alt="">
                 <img width="40" class="mx-2" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/discover.svg" alt="">
                 <img width="40" class="mx-2" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/credit-card.svg" alt="">
               </div>
               <hr class="mx-2 w-100">

               <div id="credit_payment" class="payment-block row hidden">
                 <div class="form-group col-5">
                   <input class="form-control" type="text" placeholder="CEP">
                 </div>
                 <div class="form-group col-7">
                   <input class="form-control" type="text" placeholder="City">
                 </div>
                 <div class="form-group col-12">
                   <input class="form-control" type="text" placeholder="Street Adress">
                 </div>
                 <div class="form-group col-5">
                   <input class="form-control" type="text" placeholder="Number">
                 </div>
                 <div class="form-group col-5">
                   <input class="form-control" type="text" placeholder="Complement">
                 </div>
                 <div class="form-group col-2">
                   <input class="form-control" type="text" placeholder="State">
                 </div>
                 <div class="form-group col-12">
                   <input type="text" class="form-control" placeholder="Credit Card Number">
                 </div>
                 <div class="form-group col-4">
                   <input class="form-control" type="text" placeholder="CPV">
                 </div>
                 <div class="form-group col-4">
                   <input type="text" placeholder="Month" class="form-control">
                 </div>
                 <div class="form-group col-4">
                   <input type="text" placeholder="Year" class="form-control">
                 </div>
                 <div class="mx-3 border border-dark rounded text-center w-100 py-4 bg-light">
                   <img width="60" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/sack-money.png" alt="" class="d-block m-auto">
                   <div class="d-flex align-items-center justify-content-center">
                     <img width="30" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/icons/arrow-right.svg" alt="">
                     <input class="mx-2" type="radio">
                     <p class="m-0 p-0"><strong>Yes I Want Lifetime Protection</strong></p>
                   </div>
                   <p class="small" >By placing your order today you can have a lifetime protection and replacement warranty for only an additional $19.97. This extended warranty means your product is covered for LIFE.</p>
                 </div>
                 <div class="w-100 px-3 mt-3">
                   <button class="d-block py-3 m-auto btn btn-success w-100">
                      <div class="h5 m-0 p-0">
                        YES! Send me my MagniSole!
                      </div>
                   </button>
                 </div>
               </div>
               <div class="hidden payment-block d-flex align-items-center w-100 bg-warning rounded text-center px-3 py-2 justify-content-around" id="paypal_payment">
                 <span class="h6 m-0 p-0"><b>CHECKOUT WITH</b></span>
                 <img width="130" src="https://gadgetweb-9bvb8shtos4t2j1exw.stackpathdns.com/magnisolecmsfull/img/paypal.png" alt="">
               </div>

             </div>
             <img class="d-block m-auto" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/images/trustlogo.png" alt="">
             <div class="d-flex mt-4 align-items-center">
               <img width="140" class="d-block mr-4" src="https://euqueroja.com/wp-content/plugins/Ikarus-Dev-Content/files/images/seal-.png" alt="">
               <p><b>GARANTIA DE 60 DIAS:</b> Se você comprar os PowerPés e por algum motivo não ficar satisfeito – nós te oferecemos uma garantia de 60 dias em todas as compras. Basta enviar-nos de volta o(s) item(s) para receber um reembolso completo ou troca, menos o valor do Frete!</p>
             </div>
           </div>
         </div>
         <div class="col-12">
           <div class="card px-5">
             <div class="h4 mt-5 text-center">PERGUNTAS FREQUENTES</div>
             
             <?php 
             foreach($questions as $key => $question):
                 if($question !== '' && $answers[$key] !== ''):
               $answer = $answers[$key];
               ?>

                 <div class="rounded py-2 lead px-2 px-md-4 bg-dark text-light">
                   <b><?php echo $question; ?></b>
                 </div>
                 <div class="mb-4 mt-3 ml-3 ml-md-5">
                    <?php echo $answer; ?>
                 </div>

               <?php
                 endif;
             endforeach;
             ?>
             

             
           </div>
         </div>
       </div>
     </div>
   </div>
 </main>
 
 <footer class="bg-light text-secondary mt-5 pb-5 text-center px-3">
   
   <p style="max-width: 700px; margin: auto; font-size: 14px">
      <a href="https://doseperfeita.com/Termos.htm">TERMOS E CONDIÇÕES</a> <span class="mx-2">|</span> <a href="https://doseperfeita.com/Pol%C3%ADtica%20de%20Privacidade.htm">POLÍTICA DE PRIVACIDADE</a> <span class="mx-2">|</span> <a href="https://doseperfeita.com/Contato.htm">CONTATO</a>
      <br/>
      <br/>
      <span style="font-size: 13px">
        By filling out the field, you consent for MagniSole to use automated technology, including texts and prerecorded messages, to contact you at the number and email provided about MagniSole offers.

      </span>
   </p>
   
   <img class="d-block mx-auto mt-3" src="https://images.dmca.com/Badges/dmca_protected_sml_120ac.png?ID=1b77e11b-c39c-469b-b37d-9a0c4decf08c" alt="">
   
 </footer>

  
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
  </script>
  <script>
    
var checkoutready = false;
    
    $('input[name="payment"]').on('change', (e) => {
      $('.payment-block').addClass('hidden')
      let id = $(e.target).val()
      console.log($('#'+id))
      $('#'+id).removeClass('hidden')
    })
    
    
    $(document).ready(function(){      
	  <?php echo "var variations = ".json_encode($json_variations_list).";"; ?>
	  function unselect_all(){
		  $.each($(".size-choicer"), function(i, e){
			  $(this).data("selected", -1)
			  
		  })
		  updateChoices()
	  }
	

	  $(".sizeButton").click(function(){
		 var size = $(this).parent().data("size")
		 $(this).parent().parent().data("selected", size)
		 updateChoices()
		 var variation = getVariationIdByChoices($(this));
		 if(variation !== -1){ 
      alterarProduto(variation)
     }else{
      disableCheckout()
     }
		 
	  })
	  
	  $(".selectProduct").click(function(){
		 unselect_all()
		 
	  })
	  
	  		
	  
	  function getVariationIdByChoices(e){
		  var product = $(e).closest(".product-variations")
		  var productId = $(product).data("productid")
		  //alert(productId)
		  var sizes = {"female": 0, "male": 0}
		  var error = false;
		  $.each($(product).find(".size-choicer"), function(){
			  var selected = $(this).data("selected")
			  if(selected === 0){
				  sizes.male++;
			  }else if (selected === 1){
				  sizes.female++;
			  }else{
				  error = true;
			  }
		  })
		  if(error) return -1;
		  var variationString = ""
		  if(sizes.female !== 0){
			  variationString += sizes.female + " Homem" 
		  }
		  if(sizes.female !== 0 && sizes.male !== 0) variationString += " "
		  if(sizes.male !== 0){
			  variationString += sizes.male + " Mulher" 
		  }
		  console.log(variationString)
		  return variations[productId][variationString]
	  }
	  
	  
      
	  function updateChoices(){
		$.each($(".size-choicer"), function(i, e){
			var selected = $(this).data("selected")
			$.each($(this).children("div"), function(i, e){
				var size = $(this).data("size")
				if(selected == size){
					$(this).children("button").addClass("btn-success")
				}else{
					$(this).children("button").removeClass("btn-success")
				}
				
			})
			
		})
	  }	  
	  
	  updateChoices()
	
	  var countdown = 9*60+59; // 9:59 in seconds
	  
	  var counter = setInterval(function() {
		var m = Math.floor(countdown/60);
		var s = ("0" + countdown%60).slice(-2);
		$(".countdown").text(m+":"+s); 
		if(countdown === 0){
			clearInterval(counter);
		}
		countdown--;
	  }, 1000)

      var superForm = document.querySelector('form[name="checkout"]')
      var formReplacer = document.getElementById('form-replacer')

      var customer_info = document.querySelector('#client-info-super')
      var credit_payment = document.getElementById('payment-super')


      fields = $('*[id*="billing_first_name"]:not(p), *[id*="billing_last_name"]:not(p), *[id*="billing_postcode"]:not(p),*[id*="billing_email"]:not(p), *[id*="billing_address_1"]:not(p), *[id*="billing_number"]:not(p), *[id*="billing_address_2"]:not(p), *[id*="billing_neighborhood"]:not(p), *[id*="billing_city"]:not(p), *[id*="billing_state"]:not(p), *[id*="billing_phone"]:not(p), *[id*="billing_brazil_document"]:not(p)')

      labels = fields.parent().prev().removeClass("screen-reader-text")

      fields.attr('class', 'form-control')
	  
	  function array_move(arr, old_index, new_index) {
		if (new_index >= arr.length) {
			var k = new_index - arr.length + 1;
			while (k--) {
				arr.push(undefined);
			}
		}
		arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
		return arr;
	 };



      function rebuild_field(field, label){
        let group = document.createElement('div')
        group.classList.add('form-group')
        group.classList.add('col-12')
        group.classList.add('col-md-6')
        group.appendChild(label)
        group.appendChild(field)
        return group
      }

      _fields = []
	  fields = array_move(fields, 11, 3)
	  labels = array_move(labels, 11, 3)
	  
	  //console.log(labels)
	  
	  //console.log(fields)
      for(i=0; i<fields.length; i++){
        _fields.push(rebuild_field(fields[i], labels[i]))
      }

      customer_info.innerHTML = ''
      _fields.forEach(field => {
        customer_info.appendChild(field)
      })

      order_review = document.getElementById('order_review')
      order_review.classList.add('col-12')
      credit_payment.innerHTML = ""
      credit_payment.appendChild(order_review)

      var superFormChildren = $(superForm).children()
      var hiddenDiv = document.createElement('div')
      hiddenDiv.classList.add('hidden')
      $(hiddenDiv).append(superFormChildren)
      superForm.appendChild(hiddenDiv)

      formReplacer.appendChild(superForm)
      superForm.appendChild(formReplacer.firstElementChild)
      initCepAutofill()
      disableCheckout()
    })
    
	function unhideOptions(id){
    disableCheckout()
		jQuery('.product-variations').addClass('d-none')
		jQuery('#produto'+id).removeClass('d-none')
  }
	  
    
    function alterarProduto( id ){
      //$(event.target).removeClass('btn-secondary')
      //$('.btn-primary').addClass('btn-secondary')
      //$('.btn-primary').removeClass('btn-primary')
      //$(event.target).addClass('btn-primary')
      jQuery.get('https://euqueroja.com/wp-json/ikarus/set-cart/'+(id >= 0 ? id : "") )
        .done( e => {
          jQuery(document.body).trigger("update_checkout");
          //    jQuery( document ).trigger( 'wc_update_cart' );
        enableCheckout()
        } )
    }

    
function initCepAutofill(){
    $('#billing_postcode').on('input', function(e){
        let cep = e.target.value;
        if(cep.length > 7){
            $.get('https://api.postmon.com.br/v1/cep/'+cep.replace(/\D/g,''))
            .done(e => {
                document.getElementById('billing_neighborhood').value = e.bairro
                document.getElementById('billing_city').value = e.cidade
                document.getElementById('billing_address_1').value = e.logradouro
                document.getElementById('billing_state').value = e.estado
            })
        }
    })
}

function disableCheckout(){
  $('#payment-super').addClass('disabledcheckout')
}
function enableCheckout(){
  $('#payment-super').removeClass('disabledcheckout')
}
    
    jQuery(document).ready(function() {
$("#payment_method_ebanx-credit-card-br").trigger('click')
$("#payment_method_ebanx-credit-card-br").trigger('click')
	})
    
  </script>
  
  <style>
    
.disabledcheckout:before {
    position: absolute;
    color: white;
    content: 'Escolha o tamanho';
    height: 100%;
    font-size: 30px;
    top: 0px;
    right: 0px;
    left: 0px;
    bottom: 0px;
    background-color: rgb(0,0,0,0.7);
    z-index: 1;
    font-weight: bold;
    display: flex;
    justify-content: center;
    align-items: center;
}

.disabledcheckout {
    pointer-events: none;
    position: relative;
}
  </style>
    
    
</div>


<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
<?php endif; ?>

<script>
	
	document.addEventListener('DOMContentLoaded', function() 
	{ 
	    setTimeout(function(){	    	
	    	jQuery("#place_order").html('FAZER MEU PEDIDO');
	    }, 2000);
	    
	});  

</script>

<?php wp_footer(); ?>