
jQuery(document).ready(function() 
{

    var carrinho = {
      items: [],
      pacote: null,
    };

    /* Show hide stuff */
    jQuery("#hid-one").hide();
    jQuery("#hid-two").hide();
    jQuery("#hid-three").hide();
    
    jQuery("#comp-btn-one").click(function () {
        carrinho.items = [];
        jQuery("#hid-one").toggle();

        jQuery("#hid-two").hide();
        jQuery("#hid-three").hide();

        jQuery("#hid-two").find(".variable_btn_col").toggleClass('variable_btn_col');
        jQuery("#hid-three").find(".variable_btn_col").toggleClass('variable_btn_col');
    });
    jQuery("#comp-btn-two").click(function () {
        carrinho.items = [];
        jQuery("#hid-two").toggle();

        jQuery("#hid-one").hide();
        jQuery("#hid-three").hide();

        jQuery("#hid-one").find(".variable_btn_col").toggleClass('variable_btn_col');
        jQuery("#hid-three").find(".variable_btn_col").toggleClass('variable_btn_col');
    });
    jQuery("#comp-btn-three").click(function () {
      carrinho.items = [];
      jQuery("#hid-three").toggle();

      jQuery("#hid-one").hide();
      jQuery("#hid-two").hide();

      jQuery("#hid-one").find(".variable_btn_col").toggleClass('variable_btn_col');
      jQuery("#hid-two").find(".variable_btn_col").toggleClass('variable_btn_col');
    });

    /* Toggle color class */
    jQuery('.shop').on('click', '.variation_btn', function(e) {

        e.preventDefault();
        var $this = jQuery(this);  
        jQuery(this).toggleClass('variable_btn_col');
        jQuery(this).parents(".hom-mul").find('.variation_btn').each(function(obj, i) {
            if(jQuery(this).html() != $this.html()) {
                jQuery(this).removeClass('variable_btn_col');
            }            
        });

        carrinho.items = [];

        if(jQuery("#hid-one").find(".variable_btn_col").length == 1) {            
            jQuery("#hid-one").find(".variable_btn_col").each(function(i, obj) {                
                carrinho.items.push({
                  item: 'item-' + parseInt(i+1),
                  pacote: 'pacote-1',
                  tamanho: jQuery(this).html()
                });
            });
            carrinho.pacote = 'pacote-1';
            jQuery('#exampleModalCenter').modal('show');
        } else if (jQuery("#hid-two").find(".variable_btn_col").length == 3) {                    
            jQuery("#hid-two").find(".variable_btn_col").each(function(i, obj) {
                carrinho.items.push({
                  item: 'item-' + parseInt(i+1),
                  pacote: 'pacote-2',
                  tamanho: jQuery(this).html()
                });
            });
            carrinho.pacote = 'pacote-2';
            jQuery('#exampleModalCenter').modal('show');
        } else if (jQuery("#hid-three").find(".variable_btn_col").length == 5) {                 
            jQuery("#hid-three").find(".variable_btn_col").each(function(i, obj) {                
                carrinho.items.push({
                  item: 'item-' + parseInt(i+1),
                  pacote: 'pacote-3',
                  tamanho: jQuery(this).html()
                });
            });
            carrinho.pacote = 'pacote-3';
            jQuery('#exampleModalCenter').modal('show');
        }

        console.log(carrinho);

    });

    jQuery("#ToCheckout").on('submit', function(e){        
        e.preventDefault();
        ir_para_vendas();
    });

    function ir_para_vendas() {

      var cep = document.querySelector('input#cep').value;
      var nome = document.querySelector('input#name').value;
      
      localStorage.setItem('carrinho', JSON.stringify(carrinho));
      localStorage.setItem('cep', cep);
      localStorage.setItem('nome', nome);      
      
      if(carrinho.pacote == 'pacote-1'){        
        window.location = '/powerpes/checkouts/supersola-1/';
      }else if(carrinho.pacote == 'pacote-2'){        
        window.location = '/powerpes/checkouts/supersola-2/';
      }else if(carrinho.pacote == 'pacote-3'){        
        window.location = '/powerpes/checkouts/supersola-3/';
      }
    }



});


