(function($)
{
    'use strict';

    $(function () {
        var keyValue   = $('#woocommerce_woo-moip-official_wirecard_key_account').val()
        ,   tokenValue = $('#woocommerce_woo-moip-official_wirecard_token_account').val();

        $('#wirecard-split-app').prop('disabled', true);

        if(keyValue && tokenValue){
            $('#wirecard-split-app').prop('disabled', false);
        }
    });
}(jQuery));