<?php
if ( ! function_exists( 'add_action' ) ) {
	exit(0);
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Marketplace;

$model            = new Marketplace();
$current_user     = wp_get_current_user();
$admin_user       = current_user_can('administrator');
$wirecard_user_id = get_user_meta( $current_user->ID, '_wirecard_account_id', true );
?>

<?php do_action( 'dokan_dashboard_wrap_start' ); ?>
    <div class="dokan-dashboard-wrap">
        <?php do_action( 'dokan_dashboard_content_before' ); ?>    
        <div class="dokan-dashboard-content dokan-wirecard-content">
            <article class="dokan-wirecard-area">
                <header class="dokan-dashboard-header">
                    <h1 class="entry-title"><?php echo __( 'Wirecard Account', Core::TEXTDOMAIN ); ?></h1>
                </header>
            </article>

            <?php if ( $admin_user ) : ?>
                <p class="description"><?php echo __( 'Are you the site administrator! Your linked account is the same as the WooCommerce Wirecard Brazil Official plugin.', Core::TEXTDOMAIN ); ?></p>
            <?php endif; ?>
            
            <?php if ( !$admin_user && strlen( $wirecard_user_id ) > 0 ) : ?>
                <p class="description"><?php echo __( 'Your account is already linked to Wirecard.', Core::TEXTDOMAIN ); ?></p>
            <?php endif; ?>

            <?php if ( !$admin_user && strlen( $wirecard_user_id ) == 0 ) : ?>
                <div class="dokan-page-help">
                    <?php echo __( 'Before authorizing you must have a Wirecard account.', Core::TEXTDOMAIN ); ?>
                </div>
                <a href="<?php echo esc_url( $model->get_url_connect() ); ?>"><button type="button" class="button"><?php echo __( 'Authorize', Core::TEXTDOMAIN ); ?></button></a>    
                <p class="description "><?php echo __( 'Click the button to sign in to your Wirecard account.', Core::TEXTDOMAIN ); ?></p>
            <?php endif; ?> 
        </div>
        <?php do_action( 'dokan_dashboard_content_after' ); ?>
    </div>  
<?php do_action( 'dokan_dashboard_wrap_end' );