# WooCommerce Wirecard Brazil Official

**Requires at least:** 3.7  
**Tested up to:** 5.2.1  
**Stable tag:** 1.0  

## Description

Add WooCommerce Wirecard Brazil Official payment gateway for WooCommerce

### Compatibility

WooCommerce version 2.6.x to 3.7.x

### Installation

- Install and active the plugin
- `Menu` WooCommerce -> Settings -> Checkout -> Wirecard Brazil Official
- Click to authorize the store and configure the options

### Required

- WordPress >= 3.7
- Moip account confirmed
- WooCommerce lastest version
- PHP >= 5.6

### Payment methods

- Checkout Wirecard
- Transparent checkout  
-- Boleto Bancário  
-- Cartão de Crédito  
-- Débito em Conta  