=== WooCommerce Wirecard Brazil Official ===

Contributors: daniloalvess, victorfreitas, ivaniltongomes, apiki
Tags: woocommerce, checkout, cart, wirecard, moip, gateway, payments
Requires at least: 4.0
Requires PHP: 5.6
Tested up to: 5.2.1
Stable tag: 1.3.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Official Wirecard Brazil plugin built with the best development practices.
Based on V2, new REST Moip’s API, providing more speed, safety and sales conversion.

== Description ==

Payment split NOW!

Official Wirecard Brazil plugin built with the best development practices.
Based on V2, new REST Wirecard API, providing more speed, safety and sales conversion.

= Requirements =

- PHP version 5.6 or later.
- WooCommerce version 2.6.x or later.
- Create sandbox account on Wirecard, [here](https://bem-vindo-sandbox.wirecard.com.br/ "Wirecard Sandbox").
- Create production account on Wirecard, [here](https://bem-vindo.wirecard.com.br/ "Wirecard Production").

= Payments methods =

- Credit card: Visa, Mastercard, Elo, Amex, Diners, Hiper e Hipercard.
- Itaú bank transfering.
- Billet banking.

= Benefits =

- Transparent/seamless checkout. No redirect to another page or pop up screen.
- One click buy, your client’s payment information can be reminded on a future purchase. Complete safe, PCI compliance procedure as the customer credit card information isn’t keep in the store.
- All payment status are synchronized in the store admin panel.
- Boleto bar code showed on the checkout to facilitate the payment using internet banking.
- Developed based on the best practices from Wordpress and Woocommerce  to avoid Plugin incompatibility.
- Immediate cancellation status return on the checkout page so the consumer can instantly opt to change the payment method without leaving the cart.
- Registration form adapted to the Brazilian requirements. No need to install other plugins.
- Redirect checkout as an option.

= Support =

Free support followed by Wirecard integration and partnerships team.
Support link: [Support](https://apiki.com/parceiros/wirecard)

= Transaction Fees =

Check out Wirecard’s site for transaction fees. If your store monthly volume is more than R$20.000,00, please contact us for a personalized proposal at parcerias@wirecard.com.br.

== Installation ==

1. Faça upload deste plugin em seu WordPress, e ative-o;
2. Entre no menu lateral "WooCommerce > Configurações";
3. Selecione a aba "Finalizar compra" e procure pelo menu "Wirecard Brasil Oficial";
4. Entre em sua conta Wirecard, e em seguida clique no botão "Autorizar"

== Screenshots ==
1. Home screen
2. Payment Settings
3. Credit Card Settings
4. Billet Banking Settings

== Frequently Asked Questions ==

= Onde posso obter suporte ao plugin? =

Você pode utilizar o forúm do plugin [Wirecard Brasil Oficial](https://wordpress.org/support/plugin/woo-moip-official/)

Você pode utilizar o canal de atendimento, [Suporte](https://apiki.com/parceiros/wirecard/).

= Erro ao autorizar o plugin =

Antes de autorizar, confere se os links permanentes estão como Nome do Post.

Não pode possuir nenhum parâmetro na url, exemplo: https://urldosite.com/index.php.

Se o site estiver em modo de manutenção, não funcionará a autorização.

= Não está enviando e-mail =

O plugin não envia e-mail, talvez o servidor não está configurado o envio de e-mail, 
recomendamos utilizar algum plugin de SMTP para resolver esse problema.

= Funciona com todos plugins de marketplace? =

Por enquanto nessa primeira versão por padrão funciona com o plugin de marketplace [Dokan](https://wordpress.org/plugins/dokan-lite/).
Mas já estamos fazendo integração com outros.

== Changelog ==

= 1.3.0 - 08/10/2019 =
- Criação do split de pagamento.
- Integração com o marketplace Dokan.
- Resolvendo problema com a SDK.

= 1.2.9.1 - 16/09/2019 =
- Resolvendo problema com o campo de CPF no checkout transparente.
- Atualizando SDK.
- Split de pagamento.

= 1.2.9 - 12/08/2019 =
- Adicionando campo de CPF no cartão de crédito para análise de risco.
- Adicionando checagem de parâmetro na url antes de autorizar o plugin.
- Resolvendo problema de envio de email do boleto.

= 1.2.8.2 - 28/06/2019 =
- Resolvendo problema status do pedido por boleto.

= 1.2.8.1 - 24/06/2019 =
- Atualizando url do suporte no plugin.
- Resolvendo problema do frete gratuito.
- Adicionando bandeira do cartão no admin do pedido.

= 1.2.8 - 10/05/2019 =
- Adicionando submenu na administração do plugin.
- Adicionando descrição para compra com desconto.
- Resolvendo conflito com plugin de desconto.
- Adicionando informações nos campos de boleto e cartão na página de checkout.

= 1.2.7.2 - 09/04/2019 =
- Resolvendo conflito com o plugin WooCommerce Extra Checkout Fields for Brazil.

= 1.2.7.1 - 05/04/2019 =
- Resolvendo problema na quantidade de parcelas.
- Resolvendo conflito com os campos do checkout transparente.
- Resolvendo conflito do checkout padrão.

= 1.2.7 - 04/04/2019 =
- Resolvendo problema de criação de pedido quando ocorre erro no checkout.
- Ajustando textos dos campos obrigatórios.
- Adicionando envio do boleto para o e-mail.
- Alterando o desconto Wirecard para desconto somente no boleto.
- Adicionando campo para digitar o nome do portador do cartão de crédito.
- Adicionando campo em configurações para escolher o tipo de pessoa.

= 1.2.6 - 07/02/2019 =
- Resolvendo problema de autorização.
- Alterando o desconto no boleto para desconto Wirecard.

= 1.2.5 - 20/12/2018 =
- Ajuste visual nas parcelas do checkout transparente.
- Alterando texto na página de finalização de compra.
- Resolvendo problema de notificações.

= 1.2.4 - 26/11/2018 =
- Resolvendo problema de preço nos pedidos com desconto.
- Adicionando opção de texto na aba de boleto no checkout transparente.
- Adicionando opção nas configurações para salvar o cartão de crédito.

= 1.2.3 - 21/11/2018 =
- Resolvendo problema de duplicidade no status do webhook nos pedidos.
- Resolvendo problema de alterações do preço do produto por outros plugins.
- Resolvendo problema do tipo de compra não estava aparecendo nos pedidos.
- Ajustes visual no checkout transparente.

= 1.2.2 - 05/11/2018 =
- Moip virou Wirecard, alterando a identidade visual de Moip para Wirecard.

= 1.2.1 - 09/10/2018 =
- Opção no admin do plugin para utilizar a mesma conta do Moip em vários sites.
- Botão no admin do plugin para reenviar webhooks caso esteja com o status do woocommerce/moip.
- Resolvendo o problema onde o desconto do Moip aparecia para outros meios de pagamento.
- Mostrando em pedidos do woocommerce no admin se a compra foi feita por cartão ou boleto.
- Deletando webhooks quando estiver autorizando o plugin.

= 1.2.0 - 13/08/2018 =
- Implementação de checkout transparente na página de seleção do tipo de pagamento (checkout).
- Implementando opção de definir o valor mínimo no carrinho para habilitar parcelamento.
- Implementando suporte a desconto no boleto (apenas no checkout transparente).

= 1.1.8 - 20/02/2018 =
- Ajuste na manipulação das tabs no checkout
- Implementando armazenamento da quantidade de parcelas selecionadas no checkout

= 1.1.7 - 09/02/2018 =
- Removendo jshint para atender as especificações do wordpress.org

= 1.1.6 - 11/01/2018 =
- Implementação de cancelamento do pedido após retorno do Moip;
- Ocultando opção de pagamento via débito online no checkout transparente;
- Exibindo informações sobre os webhooks (notificações) na administração.

= 1.1.5 - 12/12/2017 =
- Corrigindo bug quando selecionado apenas pessoa física no plugin WooCommerce Extra Checkout Fields For Brazil.
- Adicionando suporte a logs.

= 1.1.4 - 30/10/2017 =
- Adicionando suporte a guest checkout.

= 1.1.3 - 27/10/2017 =
- Corrigindo bug de javascript no checkout quando utilizado apenas boleto.

= 1.1.2 - 04/10/2017 =
- Renomeando arquivo principal do plugin
- Melhorias na internacionalização do arquivos

= 1.1.1 - 22/09/2017 =
- Correções de erros

= 1.1.0 - 20/09/2017 =
- Correções de erros e melhorias de performance

= 1.0.1 - 13/09/2017 =
- Implementando configuração de parcelamento por bandeira

= 1.0.0 - 30/08/2017 =
- Release inicial