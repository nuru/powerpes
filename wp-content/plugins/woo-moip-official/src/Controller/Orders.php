<?php
namespace Woocommerce\Moip\Controller;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Setting;
use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Order;

class Orders
{
    public function __construct()
    {
        $this->settings = Setting::get_instance();

        add_action( 'woocommerce_view_order', array( $this, 'add_billet_page' ), 20 );
        add_filter('woocommerce_my_account_my_orders_actions', array( $this, 'billet_button_to_list' ), 10, 2 );
        add_action( 'woocommerce_admin_order_data_after_billing_address', array( $this, 'display_order_data_in_admin' ) );
    }

    public function add_billet_page( $order_id )
    { 
        Utils::template_include( 'templates/orders', array(
            'order' => new Order( $order_id ),
        ) );
    }

    public function billet_button_to_list( $actions, $wc_order )
    { 
        $order = new Order( $wc_order->get_id() );

        if ( $order->payment_links->payBoleto->redirectHref ) {
            $actions['moip'] = array(
                'url'  => $order->payment_links->payBoleto->redirectHref . '/print',
                'name' => __( 'Billet Wirecard', Core::TEXTDOMAIN ),
            );            
        }

        return $actions;
    }

    public function display_order_data_in_admin( $wc_order ) 
    {
        $order           = new Order( $wc_order->get_id() );
        $order_id        = $wc_order->get_id();
        $moip_pay_method = get_post_meta( $order_id, '_moip_payment_type', true );
        $card_brand      = get_post_meta( $order_id, '_wbo_creditcard_brand', true );
        $message         = '';
        $cpf_title       = '';
        $cpf_holder      = '';
        $purchase        = '<span style="color:red">' . __( 'Credit Card', Core::TEXTDOMAIN ) . '</span>';
        
        if ( $moip_pay_method == '' ) {
            return;
        }

        ?>
        </div></div>
        <div class="clear"></div>
        <div class="order_data_column_container">
            <div class="order_data_column_wide">
                <h4><?php _e( 'Wirecard Brasil Oficial' ); ?></h4>
    <?php

        if ( $moip_pay_method == 'payCreditCard' ) {
    
            if ( $this->settings->is_enabled_cpf_holder() ) {
                $get_cpf_holder  = preg_replace( "/\D/", '', get_post_meta( $order_id, '_wbo_creditcard_cpf_number', true ) );
                $cpf_holder      = preg_replace( "/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $get_cpf_holder );
                $cpf_title       = __( 'CPF Number:', Core::TEXTDOMAIN );
            }
            
            $message .= sprintf(
                '<p><strong>%s: </strong><span style="color:red">%s</span></p>
                <p><strong>%s: </strong><span>%s</span></p>
                <p><strong>%s: </strong><span>%s</span>x</p>
                <p><strong>%s: </strong><span>%s</span></p>
                <p><strong>%s </strong><span>%s</span></p>',
                __( 'Payment by', Core::TEXTDOMAIN ),
                __( 'Credit Card', Core::TEXTDOMAIN ),
                __( 'Brand', Core::TEXTDOMAIN ),
                $card_brand,
                __( 'Installments', Core::TEXTDOMAIN ),
                $order->installments,
                __( 'Wirecard Order ID', Core::TEXTDOMAIN ),
                $order->resource_id,
                $cpf_title,
                $cpf_holder

            );
        }

        if ( $moip_pay_method == 'payBoleto' ) {

            $message .= sprintf(
                '<p><strong>%s: </strong><span style="color:red">%s</span></p>
                <p><strong>%s: </strong><a href="%s" target="_blank" class="admin-payment-link">%s</a></p>
                <p><strong>%s: </strong><span>%s</span></p>',
                __( 'Payment by', Core::TEXTDOMAIN ),
                __( 'Bank Billet', Core::TEXTDOMAIN ),
                __( 'Billet Link', Core::TEXTDOMAIN ),
                $order->payment_links->payBoleto->redirectHref . '/print',
                __( 'Print', Core::TEXTDOMAIN ),
                __( 'Wirecard Order ID', Core::TEXTDOMAIN ),
                $order->resource_id
            );

        }

        echo $message;

    }
}