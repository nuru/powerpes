<?php
namespace Woocommerce\Moip\Controller;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Setting;

use WC_Order;

class Payment_Splits
{
    public function __construct()
	{
        $setting = Setting::get_instance();

        if ( $setting->is_wirecard_payment_split() ) {
            add_filter( 'apiki_moip_create_order', array( $this, 'add_split_payment' ), 10, 2 );
        }
    }

    public function add_split_payment( $moip_order, WC_Order $order )
	{
        $percentage  = $this->get_percentage_comission();
        $sellers     = $this->get_sellers( $order );
        $sellers_arr = [];

        foreach ( $sellers as $seller ) {
            $positions      = $this->get_position_products_vendor( $seller, $order );
            $value_shipping = 0;
            $count          = 0;
            $seller_obj     = new \stdClass();

            foreach ( $order->get_items('shipping') as $ship ) {
                if ( in_array( $count, $positions ) ) {
                    $value_shipping += $ship->get_total();
                }
                $count++;
            }

            $count                      = 0;
            $value_total                = 0;
            $seller_obj->id             = $seller;
            $seller_obj->value_shipping = $value_shipping;
            $seller_obj->moip_id        = $this->get_moip_id( $seller, false );

            foreach ( $order->get_items() as $item ) {
                if ( in_array( $count, $positions ) ) {
                    $value_total += $item->get_total();
                }
                $count++;
            }

            $seller_obj->valor_product = $value_total;

            array_push( $sellers_arr, $seller_obj );
        }

        $count = 0;
        foreach ( $sellers_arr as $seller_arr ) {
            $percentage_vendor       = ( 100 - $percentage) / 100;
            $shipping                = isset( $seller_arr->shipping ) ? $seller_arr->shipping : 0;
            $value                   = ( $seller_arr->valor_product * $percentage_vendor ) + $shipping;
            $seller_arr->value_total = $value + $seller_arr->value_shipping;
            $moip_order->addReceiver( 
                $seller_arr->moip_id, 
                'SECONDARY',
                Utils::format_order_price( $seller_arr->value_total ), 
                null, 
                $count == 0 ? true : false 
            );

            $count++;
        }

        return $moip_order;
    }
    
    private function get_percentage_comission()
    {
        $options = get_option( 'dokan_selling', array() );
        return $options['admin_percentage'];
    }

    private function get_sellers( WC_Order $order )
    {
        $sellers = [];

        foreach ( $order->get_items() as $item ) {
            $seller_id      =  get_post_field( 'post_author', $item->get_product_id() );
            $vendor_moip_id = get_user_meta( $seller_id, '_wirecard_account_id', true );
            if( ! in_array( $seller_id ,$sellers ) && $vendor_moip_id <> '' )
                array_push( $sellers, $seller_id );
        }
        return $sellers;
    }

    private function get_position_products_vendor( $vendor, WC_Order $order )
    {
        $positions = [];
        $pos       = 0;

        foreach ( $order->get_items() as $item ){
            if ( $vendor == get_post_field( 'post_author', $item->get_product_id() ) ) {
                array_push( $positions, $pos );
           }
           $pos++;
        }
        return $positions;
    }

    private function  get_moip_id( $product_id, $is_product = true ) 
    {
        $vendor_id = get_post_field( 'post_author', $product_id );
        
        if ( ! $is_product )
            $vendor_id      = $product_id;
            $vendor         = get_userdata( $vendor_id );
            $vendor_moip_id = get_user_meta( $vendor->ID, '_wirecard_account_id', true);
        return $vendor_moip_id;
    }
}