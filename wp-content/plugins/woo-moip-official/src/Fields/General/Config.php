<?php
namespace Woocommerce\Moip\Fields\General;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Custom_Gateway;

//Views
use Woocommerce\Moip\View;

class Config
{
	public static function section_auth()
	{
		return array(
			'title'       => __( 'APP authorization', Core::TEXTDOMAIN ),
			'type'        => 'title',
			'description' => self::get_app_status(),
		);
	}

	public static function get_app_status()
	{
		$model = new Custom_Gateway();

		$class        = 'app-not-authorized';
		$status       = __( 'Not authorized', Core::TEXTDOMAIN );
		$description  = __( 'To make your sales, you must authorize this application on Wirecard.', Core::TEXTDOMAIN );
		$via          = '';
		$wclient_id   = '';
		$acc_wirecard = '';

		if ( $model->settings->authorize_data ) {
			$class         = 'app-authorized';
			$status        = __( 'Authorized', Core::TEXTDOMAIN );
			$via           = sprintf( 'via %s', strtoupper( $model->settings->authorize_mode ) );
			$wclient_id    = $model->settings->authorize_data->moipAccount->id;
			$acc_wirecard  = Utils::get_wirecard_account( $wclient_id );
			$acc_email     = isset( $acc_wirecard['email']['address'] ) ? $acc_wirecard['email']['address'] : '';
		}

		return sprintf(
			'%s<br><strong>Status: </strong><span class="%s">%s</span> %s <br>
			<strong>ID: </strong><span>%s</span><br>
			<strong>Email: </strong><span>%s</span>%s',
			$description,
			$class,
			$status,
			$via,
			$wclient_id,
			$acc_email,
			self::get_authorized_app_btn()
		);
	}

	public static function get_authorized_app_btn()
	{
		$model = new Custom_Gateway();

		$title = __( 'Authorize App', Core::TEXTDOMAIN );
		$class = 'button-primary';

		if ( $model->settings->authorize_data ) {
			$title = __( 'New authorize', Core::TEXTDOMAIN );
			$class = '';
		}

		return sprintf(
			'<p>
				<a href="#"
				   class="button %s"
				   id="oauth-app-btn">
					%s
				</a>
			</p>',
			$class,
			$title
		);
	}

	public static function section_settings()
	{
		return array(
			'title' => __( 'Settings', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
	}

	public static function section_payment_settings()
	{
		return array(
			'title' => __( 'Payment settings', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
	}

	public static function field_enabled()
	{
		return array(
			'title'   => __( 'Enable', Core::TEXTDOMAIN ),
			'type'    => 'checkbox',
			'label'   => __( 'Enable payment', Core::TEXTDOMAIN ),
			'default' => 'no',
		);
	}

	public static function field_title()
	{
		return array(
			'title'       => __( 'Title', Core::TEXTDOMAIN ),
			'description' => __( 'This the title which the user sees during checkout.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'default'     => __( 'Wirecard', Core::TEXTDOMAIN ),
		);
	}

	public static function field_description()
	{
		return array(
			'title'   => __( 'Description', Core::TEXTDOMAIN ),
			'default' => __( 'Pay with Wirecard', Core::TEXTDOMAIN ),
		);
	}

	public static function field_invoice_name()
	{
		return array(
			'title'             => __( 'Invoice name', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'placeholder'       => __( 'Maximum of 13 characters', Core::TEXTDOMAIN ),
			'description'       => __( 'It allows the shopkeeper to send a text of up to 13 characters that will be printed on the bearer\'s invoice, next to the shop identification, respecting the length of the flags.', Core::TEXTDOMAIN ),
			'custom_attributes' => array(
				'data-action'    => 'invoice-name',
				'data-element'   => 'validate',
				'maxlength'      => 13,
				'data-error-msg' => __( 'This field is required.', Core::TEXTDOMAIN ),
			),
		);
	}

	public static function field_invoice_prefix()
	{
		return array(
			'title'       => __( 'Invoice prefix', Core::TEXTDOMAIN ),
			'default'     => 'WC-',
			'desc_tip'    => true,
			'description' => __( 'Enter a prefix for your invoice numbers. If you use your Wirecard account for multiple stores, make sure this prefix is unique because Wirecard will not allow orders with the same invoice number.', Core::TEXTDOMAIN ),
		);
	}

	public static function field_payment_type()
	{
		return array(
			'type'              => 'select',
			'title'             => __( 'Payment Type', Core::TEXTDOMAIN ),
			'class'             => 'wc-enhanced-select',
			'default'           => 'default_checkout',
			'custom_attributes' => array(
				'data-element'  => 'checkout',
				'data-action'   => 'checkout-type',
			),
			'options' => array(
				'default_checkout'     => __( 'Default Checkout', Core::TEXTDOMAIN ),
				'transparent_checkout' => __( 'Transparent Checkout', Core::TEXTDOMAIN ),
				'moip_checkout'        => __( 'Wirecard Checkout', Core::TEXTDOMAIN ),
			),
		);
	}

	public static function field_public_key()
	{
		return array(
			'title'             => __( 'Public Key', Core::TEXTDOMAIN ),
			'type'              => 'textarea',
			'css'               => 'height: 200px',
			'custom_attributes' => array(
				'data-element'   => 'validate',
				'data-field'     => 'public-key',
				'data-error-msg' => __( 'This field is required.', Core::TEXTDOMAIN ),
			),
			'description' => __( 'Allows credit card data to be sent encrypted, generating more security in your transactions.OBS: You can not change this field manually.', Core::TEXTDOMAIN )
		);
	}

	public static function field_billet_banking()
	{
		return array(
			'title'   => __( 'Billet Banking', Core::TEXTDOMAIN ),
			'type'    => 'checkbox',
			'label'   => __( 'Enable Billet Banking', Core::TEXTDOMAIN ),
			'default' => 'yes',
			'custom_attributes' => array(
				'data-field' => 'wbo-billet-banking',
			),
		);
	}

	public static function field_credit_card()
	{
		return array(
			'title'   => __( 'Credit Card', Core::TEXTDOMAIN ),
			'type'    => 'checkbox',
			'label'   => __( 'Enable Credit Card', Core::TEXTDOMAIN ),
			'default' => 'yes',
			'custom_attributes' => array(
				'data-field' => 'wbo-credit-card',
			),
		);
	}

	public static function field_banking_debit()
	{
		return array(
			'title'   => __( 'Banking Debit', Core::TEXTDOMAIN ),
			'type'    => 'checkbox',
			'label'   => __( 'Enable Banking Debit', Core::TEXTDOMAIN ),
			'default' => 'yes',
			'custom_attributes' => array(
				'data-field' => 'debit',
			),
		);
	}

	public static function section_tools()
	{
		return array(
			'title' => __( 'Checkout', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
	}

	public static function field_person_type()
	{
		$model = new Custom_Gateway();

		return array(
			'type'              => 'select',
			'title'             => __( 'Person Type', Core::TEXTDOMAIN ),
			'description'       => __( 'Choose the type of person (Individuals and/or Legal Person) to appear on the checkout page.', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'class'             => 'wc-enhanced-select',
			'default'           => 0,
			'options'     => $model->get_persontype_options(),
			'custom_attributes' => array(
				'data-action' => 'checkout-person-type',
			),
		);
	}

	public static function section_notification()
	{
		return array(
			'title'       => __( 'Notifications', Core::TEXTDOMAIN ),
			'type'        => 'title',
			'description' => self::webhook_fields_notifications()
		);
	}

	public static function webhook_fields_notifications()
	{
		$model      = new Custom_Gateway();
		$all_token  = '';

		if ( $model->settings->authorize_data ) {
			$all_token    .= View\Custom_Gateways::render_notification_webhook( $model->settings );
		}

		return $all_token;
	}

	public static function get_fields()
	{
		return array(
			'auth'                     => self::section_auth(),
			'section_settings'         => self::section_settings(),
			'enabled'                  => self::field_enabled(),
			'title'                    => self::field_title(),
			'description'              => self::field_description(),
			'invoice_name'             => self::field_invoice_name(),
			'invoice_prefix'           => self::field_invoice_prefix(),
			'section_payment_settings' => self::section_payment_settings(),
			'public_key'               => self::field_public_key(),
			'payment_api'              => self::field_payment_type(),
			'billet_banking'           => self::field_billet_banking(),
			'credit_card'              => self::field_credit_card(),
			'banking_debit'            => self::field_banking_debit(),
			'section_tools'            => self::section_tools(),
			'field_person_type'        => self::field_person_type(),
			'section_notification'     => self::section_notification(),
		);
	}
}