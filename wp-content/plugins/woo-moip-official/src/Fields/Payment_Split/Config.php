<?php
namespace Woocommerce\Moip\Fields\Payment_Split;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Custom_Gateway;

//Views
use Woocommerce\Moip\View;

class Config
{
    public static function section_payment_split()
	{
		return array(
			'title'       => __( '', Core::TEXTDOMAIN ),
			'type'        => 'title',
			'description' => sprintf(
				'%s', 
				self::split_message_soon() 
			),
		);
    }

    public static function split_message_soon()
	{
        $url_plugin  = 'https://wordpress.org/plugins/dokan-lite/';
		$url_support = 'https://apiki.com/parceiros/wirecard/';

        return sprintf(
            '%s
            <a href="%s">%s</a> %s
            %s
            <a href="%s">%s</a>.',
            __( 'Estamos utilizando por enquanto o plugin de marketplace', Core::TEXTDOMAIN ),
            esc_url( $url_plugin ),
            __( 'Dokan Lite', Core::TEXTDOMAIN ),
            __( 'por padrão.', Core::TEXTDOMAIN ),
            __( 'Se utilizar outro plugin nos envie uma mensagem pelo', Core::TEXTDOMAIN ),
            esc_url( $url_support ),
            __( 'Suporte Apiki', Core::TEXTDOMAIN )
        );
    }

    public static function wirecard_checkbox_payment_split()
	{
		return array(
			'title'       => __( 'Payment Split', Core::TEXTDOMAIN ),
			'type'        => 'checkbox',
			'label'       => __( 'Enable payment split', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'description' => __( 'After generating the app you need to enable split to work.', Core::TEXTDOMAIN ),
			'desc_tip'    => false,
			'custom_attributes' => array(
				'data-field' => 'render-wirecard-payment-split',
			),
		);
	}
	
	public static function marketplace_options_type()
	{
		return array(
			'type'              => 'select',
			'title'             => __( 'Marketplace Type', Core::TEXTDOMAIN ),
			'class'             => 'wc-enhanced-select',
			'default'           => 'default_marketplace',
			'custom_attributes' => array(
				'data-element'  => 'marketplace',
				'data-action'   => 'marketplace-type',
			),
			'options' => array(
				'default_marketplace'  => __( 'Select your Marketplace', Core::TEXTDOMAIN ),
				'dokan_marketplace'    => __( 'Dokan', Core::TEXTDOMAIN ),
			),
		);
	}

    public static function wirecard_token_account()
	{
		return array(
			'title'       => __( 'Wirecard Token', Core::TEXTDOMAIN ),
			'type'        => 'text',
			'desc_tip'    => false,
			'default'     => '',
			'description' => sprintf( 
				__( 'Copy and paste wirecard token, where to find: %s' , Core::TEXTDOMAIN ), 
				self::get_wirecard_token() 
			),
		);
	}
	
	public static function get_wirecard_token()
	{
		return '<a href="' . Utils::get_url_endpoint_connect() . '">' 
			. __( 'Settings &gt; Access Keys', Core::TEXTDOMAIN ) . '</a> '
			. __( 'In Authentication Key copy TOKEN.', Core::TEXTDOMAIN );
    }
    
    public static function wirecard_key_account()
	{
		return array(
			'title'       => __( 'Wirecard Key', Core::TEXTDOMAIN ),
			'type'        => 'text',
			'desc_tip'    => false,
			'default'     => '',
			'description' => sprintf( 
				__( 'Copy and paste wirecard key, where to find: %s' , Core::TEXTDOMAIN ), 
				self::get_wirecard_key() 
			),
		);
	}

	public static function get_wirecard_key()
	{
		return '<a href="' . Utils::get_url_endpoint_connect() . '">' 
			. __( 'Settings &gt; Access Keys', Core::TEXTDOMAIN ) . '</a> '
			. __( 'In Authentication Key click: [click here to show key] copy KEY.', Core::TEXTDOMAIN );
    }
    
    public static function section_wirecard_app()
	{
		return array(
			'title' => __( 'Wirecard APP', Core::TEXTDOMAIN ),
            'type'  => 'title',
            'description' => sprintf( 
				'%s',
				self::generate_wirecard_app()
			),
		);
    }

	public static function generate_wirecard_app()
	{
		$button_notification = View\Payment_Splits::render_button_wirecard_split();

		return $button_notification;
    }
    
    public static function section_app_info()
	{
		return array(
			'title'       => __( 'App Info', Core::TEXTDOMAIN ),
			'type'        => 'title',
			'description' => self::get_app_info()
		);
	}

	public static function get_app_info()
	{
		$model      = new Custom_Gateway();
		$all_token  = '';

		if ( $model->settings->authorize_data ) {
			$all_token .= View\Payment_Splits::render_info_wirecard_app( $model->settings );
		}

		return $all_token;
	}
    
	public static function get_fields()
	{
		return array(
			'section_payment_split'        => self::section_payment_split(),
            'wirecard_payment_split'       => self::wirecard_checkbox_payment_split(),
			//'marketplace_options_type'     => self::marketplace_options_type(),
            'wirecard_token_account'       => self::wirecard_token_account(),
            'wirecard_key_account'         => self::wirecard_key_account(),
            'section_wirecard_app'         => self::section_wirecard_app(),
			'section_app_info'             => self::section_app_info()
        );
	}
}