<?php
namespace Woocommerce\Moip\Fields\Tools;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;

//Views
use Woocommerce\Moip\View;

class Config
{
    public static function section_tools()
	{
		return array(
			'title' => __( 'Tools', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
    }

    public static function field_enabled_logs()
	{
		return array(
			'title'       => __( 'Logs', Core::TEXTDOMAIN ),
			'type'        => 'checkbox',
			'label'       => __( 'Enable', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'description' => sprintf( 
				__( 'To View the logs click the link: %s' , Core::TEXTDOMAIN ), 
				self::moip_oficial_log_view() 
			),
		);
    }
    
	public static function moip_oficial_log_view() 
	{
		return '<a href="' . esc_url( 
            admin_url( 'admin.php?page=wc-status&tab=logs&log_file=wirecard-brazil-official-' 
            . sanitize_file_name( wp_hash( Core::SLUG ) ) . '.log' ) ) . '">' 
            . __( 'System Status &gt; Logs', Core::TEXTDOMAIN ) . '</a>';
    }
    
    public static function section_webhook()
	{
		return array(
			'title'       => __( 'Webhooks', Core::TEXTDOMAIN ),
			'type'        => 'title',
			'description' => sprintf( 
				'%s',
				self::send_webhook()
			),
		);
	}

	public static function send_webhook()
	{
		$button_notification = View\Custom_Gateways::render_button_webhook_send();

		return $button_notification;
	}
    
	public static function get_fields()
	{
		return array(
            'section_tools'   => self::section_tools(),
            'enable_logs'     => self::field_enabled_logs(),
            'section_webhook' => self::section_webhook()
        );
	}
}