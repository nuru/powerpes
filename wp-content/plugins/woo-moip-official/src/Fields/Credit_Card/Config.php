<?php
namespace Woocommerce\Moip\Fields\Credit_Card;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Setting;
use Woocommerce\Moip\Model\Custom_Gateway;

class Config
{
	public static function not_found()
	{
		return array(
			'title' => __( 'Enable credit card option!', Core::TEXTDOMAIN ),
			'type'  => 'title',
			'description' => sprintf( 
				__( 'Click the link to go back and activate your credit card: %s' , Core::TEXTDOMAIN ), 
				self::get_general_url() 
			),
		);
	}

	public static function not_found_cc()
	{
		return array(
			'title' => __( 'Credit card setup is not yet available for the wirecard checkout!', Core::TEXTDOMAIN ),
			'type'  => 'title'
		);
	}

	public static function get_general_url()
	{
		$url = admin_url( 'admin.php?page=wc-settings&tab=checkout&section=woo-moip-official&wirecard-tab=wbo-general' );
		return '<a href="' . esc_url( $url ) . '">' . __( 'General', Core::TEXTDOMAIN ) . '</a>';
	}

	public static function section_installments()
	{
		return array(
			'title' => __( 'Credit Card Settings', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
    }
    
    public static function field_wirecard_cpf_holder()
	{
		return array(
			'title'       => __( 'CPF', Core::TEXTDOMAIN ),
			'type'        => 'checkbox',
			'label'       => __( 'Enable CPF field', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'description' => __( 'Add CPF field to credit card tab for risk analysis.(for transparent checkout only).', Core::TEXTDOMAIN ),
			'desc_tip'    => false,
			'custom_attributes' => array(
				'data-field' => 'render-cpf-field',
			),
		);
	}

	public static function save_credit_card_option()
    {
    	return array(
			'title'       => __( 'Save Credit Card', Core::TEXTDOMAIN ),
			'description' => __( 'Show option to save the card number in transparent checkout.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'type'        => 'checkbox',
			'label'       => __( 'Show Option', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'custom_attributes' => array(
				'data-field' => 'credit-card-option',
			),
		);
    }

	public static function field_credit_card_installments( $field )
	{
		$model        = new Custom_Gateway();
		$installments = array();

		$installments['enabled'] = array(
			'title'             => __( 'Installments settings', Core::TEXTDOMAIN ),
			'type'              => 'checkbox',
			'label'             => __( 'Enable Installments settings', Core::TEXTDOMAIN ),
			'default'           => 'no',
			'custom_attributes' => array(
				'data-field' => 'installments',
			),
		);

		$installments['installment'] = array(
			'title'       => __( 'Minimum installment', Core::TEXTDOMAIN ),
			'type'        => 'text',
			'description' => __( 'Amount of the minimum installment to be applied to the card.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'placeholder' => '0,00',
			'custom_attributes' => array(
				'data-mask'         => '##0,00',
				'data-mask-reverse' => true,
			),
		);

		$installments['maximum'] = array(
			'title'       => __( 'Maximum installments number', Core::TEXTDOMAIN ),
			'type'        => 'select',
			'description' => __( 'Force a maximum number of installments for payment.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'default'     => 12,
			'options'     => $model->get_installment_options(),
			'custom_attributes' => array(
				'data-action' => 'installments-maximum',
			),
		);

		$installments['general'] = array(
			'title'       => __( 'Interest per installment', Core::TEXTDOMAIN ),
			'type'        => 'installments',
			'description' => __( 'Define interest for each installment.', Core::TEXTDOMAIN ),
			'desc_tip'    => false,
		);

		$installments['interest'] = array(
			'title'       => __( 'Interest', Core::TEXTDOMAIN ),
			'type'        => 'text',
			'description' => __( 'Interest to be applied to the installment.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'placeholder' => '0,00',
		);

		return $installments[ $field ];
	}

	public static function get_fields()
	{
		$settings = Setting::get_instance();		
		$values   = array(
			'not_found'  => self::not_found(),
		);

		if ( $settings->is_checkout_wirecard() ) {
			$values = array(
				'not_found_cc'  => self::not_found_cc(),
			);
			
			return $values;
		}

		if ( $settings->is_active_credit_card() ) {
			$values   = array(
				'section_installments'  => self::section_installments(),
                //'save_credit_card'      => self::save_credit_card_option(),
                'wirecard_cpf_holder'   => self::field_wirecard_cpf_holder(),
				'installments_enabled'  => self::field_credit_card_installments( 'enabled' ),
				'installments_minimum'  => self::field_credit_card_installments( 'installment' ),
				'installments_maximum'  => self::field_credit_card_installments( 'maximum' ),
				'installments'          => self::field_credit_card_installments( 'general' ),
			);
		}

		return $values;
	}
}