<?php
namespace Woocommerce\Moip\Fields\Billet;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Setting;

class Config
{
	public static function not_found()
	{
		return array(
			'title' => __( 'Ativar opção de boleto bancário!', Core::TEXTDOMAIN ),
			'type'  => 'title',
			'description' => sprintf( 
				__( 'To View the logs click the link: %s' , Core::TEXTDOMAIN ), 
				self::get_general_url() 
			),
		);
	}

	public static function get_general_url()
	{
		$url = admin_url( 'admin.php?page=wc-settings&tab=checkout&section=woo-moip-official&wirecard-tab=wbo-general' );
		return '<a href="' . esc_url( $url ) . '">' . __( 'General', Core::TEXTDOMAIN ) . '</a>';
	}

	public static function section_billet_settings()
	{
		return array(
			'title' => __( 'Billet Settings', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
	}

	public static function send_wirecard_billet_email()
	{
		return array(
			'title'       => __( 'Send Billet to Email', Core::TEXTDOMAIN ),
			'type'        => 'checkbox',
			'label'       => __( 'Enable billet sending for email', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'description'       => __( 'Enabling the billet link will be sent to the buyer email.', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'custom_attributes' => array(
				'data-field'  => 'send-email-field',
			),
		);
	}

	public static function render_billet_description()
    {
    	return array(
			'title'             => __( 'Description Billet Checkout', Core::TEXTDOMAIN ),
			'description'       => __( 'Add description in billet option in transparent checkout. If leave blank will show the default text.', Core::TEXTDOMAIN ),
			'type'              => 'textarea',
			'placeholder'       => __( 'Default text: The order will be confirmed only after confirmation of payment.', Core::TEXTDOMAIN ),
			'css'               => 'height: 100px',
			'custom_attributes' => array(
				'data-field'        => 'billet-discount',
			),
		);
	}
	
	public static function field_billet_deadline_days()
	{
		return array(
			'title'             => __( 'Number of Days', Core::TEXTDOMAIN ),
			'description'       => __( 'Days of expiry of the billet after printed.', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'placeholder'       => 5,
			'default'           => 5,
			'custom_attributes' => array(
				'data-field' => 'billet',
			),
		);
	}

	public static function field_billet_instructions( $line )
	{
		$instructions = array(
			1 => array(
				'title'       => __( 'Instruction Line 1', Core::TEXTDOMAIN ),
				'type'        => 'text',
				'description' => __( 'First line instruction for the billet.', Core::TEXTDOMAIN ),
				'desc_tip'    => true,
			),
			2 => array(
				'title'       => __( 'Instruction Line 2', Core::TEXTDOMAIN ),
				'type'        => 'text',
				'description' => __( 'Second line instruction for the billet.', Core::TEXTDOMAIN ),
				'desc_tip'    => true,
			),
			3 => array(
				'title'       => __( 'Instruction Line 3', Core::TEXTDOMAIN ),
				'type'        => 'text',
				'description' => __( 'Third line instruction for the billet.', Core::TEXTDOMAIN ),
				'desc_tip'    => true,
			),
		);

		return $instructions[ $line ];
	}

	public static function field_billet_logo_url()
	{
		return array(
			'title'       => __( 'Custom Logo URL', Core::TEXTDOMAIN ),
			'type'        => 'text',
			'description' => __( 'URL of the logo image to be shown on the billet.', Core::TEXTDOMAIN ),
			'desc_tip'    => true,
			'default'     => ''
		);
	}

	public static function section_discount()
	{
		return array(
			'title' => __( 'Discount Setting', Core::TEXTDOMAIN ),
			'type'  => 'title',
		);
	}

	public static function field_enabled_discount()
	{
		return array(
			'title'       => __( 'Wirecard Discount', Core::TEXTDOMAIN ),
			'type'        => 'checkbox',
			'label'       => __( 'Enable Billet Discount', Core::TEXTDOMAIN ),
			'default'     => 'no',
			'description'       => __( 'Enabling the discount will apply to billet.', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'custom_attributes' => array(
				'data-field'  => 'enable-discount-field',
			),
		);
	}

	public static function wirecard_discount_name()
    {
    	return array(
			'title'             => __( 'Discount Name', Core::TEXTDOMAIN ),
			'description'       => __( 'Enter the name of the discount (Default name if blank: Wirecard Discount).', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'type'              => 'text',
			'placeholder'       => __( 'Wirecard Discount', Core::TEXTDOMAIN ),
			'custom_attributes' => array(
				'data-field'  => 'wbo-billet-discount',
			),
		);
	}
	
	public static function wirecard_discount_number()
    {
    	return array(
			'title'             => __( 'Discount Amount', Core::TEXTDOMAIN ),
			'description'       => __( 'Enter the discount amount for the bank slip (Example: 10%).', Core::TEXTDOMAIN ),
			'desc_tip'          => true,
			'type'              => 'text',
			'placeholder'       => '0,00',
			'custom_attributes' => array(
				'data-mask'         => '##0,00%',
				'data-mask-reverse' => true,
				'data-field'        => 'wbo-billet-discount',
			),
		);
    }

	public static function get_fields()
	{
		$settings = Setting::get_instance();
		$values   = array(
			'not_found'  => self::not_found(),
		);

		if ( $settings->is_checkout_default() ) {
			$values = array(
				'section_billet_settings'    => self::section_billet_settings(),
				'render_billet_description'  => self::render_billet_description(),
				'billet_deadline_days'       => self::field_billet_deadline_days(),
				'billet_instruction_line1'   => self::field_billet_instructions( 1 ),
				'billet_instruction_line2'   => self::field_billet_instructions( 2 ),
				'billet_instruction_line3'   => self::field_billet_instructions( 3 ),
				'billet_logo'                => self::field_billet_logo_url()			
			);

			return $values;
		}

		if ( $settings->is_checkout_wirecard() ) {
			$values = array(
				'section_billet_settings'    => self::section_billet_settings(),
				'render_billet_description'  => self::render_billet_description(),
				'billet_deadline_days'       => self::field_billet_deadline_days(),
				'billet_instruction_line1'   => self::field_billet_instructions( 1 ),
				'billet_instruction_line2'   => self::field_billet_instructions( 2 ),
				'billet_instruction_line3'   => self::field_billet_instructions( 3 ),
				'billet_logo'                => self::field_billet_logo_url()			
			);

			return $values;
		}

		if ( $settings->is_active_billet_banking() ) {
			$values = array(
				'section_billet_settings'    => self::section_billet_settings(),
				'send_wirecard_billet_email' => self::send_wirecard_billet_email(),
				'render_billet_description'  => self::render_billet_description(),
				'billet_deadline_days'       => self::field_billet_deadline_days(),
				'billet_instruction_line1'   => self::field_billet_instructions( 1 ),
				'billet_instruction_line2'   => self::field_billet_instructions( 2 ),
				'billet_instruction_line3'   => self::field_billet_instructions( 3 ),
				'billet_logo'                => self::field_billet_logo_url(),
				'section_discount'           => self::section_discount(),
				'field_enabled_discount'     => self::field_enabled_discount(),
				'wirecard_discount_name'     => self::wirecard_discount_name(),
				'wirecard_discount_number'   => self::wirecard_discount_number(),			
			);
		}

		return $values;
	}
}