<?php
namespace Woocommerce\Moip\Model;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Setting;

class Checkout
{
	private $ID;

	public function __construct( $ID = false )
	{
		if ( false !== $ID ) {
			$this->ID = absint( $ID );
		}
	}

	public function get_order()
	{
		return new Order( $this->ID );
	}

	public function prepare_fields( $form_data )
	{
		if ( empty( $form_data ) ) {
			return false;
		}

		$fields = array();

		foreach ( $form_data as $data ) {

			if ( ! isset( $data['name'] ) || ! isset( $data['value'] ) ) {
				continue;
			}

			if ( empty( $data['value'] ) ) {
				continue;
			}

			$fields[ $data['name'] ] = Utils::rm_tags( $data['value'], true );

			if ( $data['name'] == 'card_number' ) {
				$fields[ $data['name'] ] = Utils::format_document( $data['value'] );
			}

			if ( $data['name'] == 'card_expiry' ) {
				$expiry_pieces               = explode( '/',  $data['value'] );
				$fields['card_expiry_month'] = trim( $expiry_pieces[0] );
				$fields['card_expiry_year']  = trim( $expiry_pieces[1] );
			}
		}

		return $fields;
	}

	public static function wbo_validate_credit_card() 
	{
		$fields      = Utils::post( 'moip_fields', false );
		$setting     = Setting::get_instance();
		$card_number = str_replace(' ','', $fields['card_number']);
		$brand       = 'undefined';

		// Brands regex
		$brands = array(
			'mastercard' => '/^(5[1-5]\d{4}|677189)\d{10}$/',
			'diners'     => '/^3(0[0-5]|[68]\d)\d{11}$/',
			'discover'   => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
			'elo'        => '/^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})$/',
			'visa'       => '/^4\d{12}(\d{3})?$/',
			'amex'       => '/^3[47]\d{13}$/',
			'hipercard'  => '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
			'maestro'    => '/^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/',
		);
		
		foreach ( $brands as $_brand => $regex ) {
			if ( preg_match( $regex, $card_number ) ) {
				$brand = $_brand;
				break;
			}
		}

		if ( $setting->is_checkout_transparent() ) {
			if ( $brand === 'undefined' && ! empty($fields['card_number']) ) {
				wc_add_notice( __( 'Wirecard: Invalid credit card number.', Core::TEXTDOMAIN ), 'error' );
			}
	
			if ( ! $fields['card_holder'] ) {
				wc_add_notice( __( 'Wirecard: Card Holder Empty', Core::TEXTDOMAIN ), 'error' );
			}
	
			if ( ! $fields['card_number'] ) {
				wc_add_notice( __( 'Wirecard: Card Number Empty', Core::TEXTDOMAIN ), 'error' );
			}
	
			if ( ! $fields['card_expiry'] ) {
				wc_add_notice( __( 'Wirecard: Card Expiry Empty', Core::TEXTDOMAIN ), 'error' );
			}
	
			if ( ! $fields['card_cvc'] ) {
				wc_add_notice( __( 'Wirecard: Card CVC Empty', Core::TEXTDOMAIN ), 'error' );
            }
            
            if ( $setting->is_enabled_cpf_holder() ) {
                if ( ! $fields['cpf_holder'] ) {
                    wc_add_notice( __( 'Wirecard: CPF Number Empty', Core::TEXTDOMAIN ), 'error' );
                }
            }
	
			if ( ! $fields['installments'] ) {
				wc_add_notice( __( 'Wirecard: Card Installments Empty', Core::TEXTDOMAIN ), 'error' );
			}
		}
		
	}
}