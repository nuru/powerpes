<?php
namespace Woocommerce\Moip\Model;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Custom_Gateway;
use Woocommerce\Moip\Helper\Utils;

class Payment_Split
{
    public function create_wirecard_split_app()
    {
        $model            = new Custom_Gateway();
        $wirecard_token   = $model->settings->wirecard_token_account;
        $wirecard_key     = $model->settings->wirecard_key_account;
        $credentials      = base64_encode( $wirecard_token.':'.$wirecard_key );
        $notification_url = Utils::get_url_endpoint() . '/v2/channels/';
        $redirect_uri     = rest_url( Core::SPLIT_REST_ROUTE.'/callback' );

        $header = array(
            'authorization: Basic '. $credentials,
            'content-type: application/json'
        );

        $data_body = array( 
            'name'        => __( 'Wirecard Brazil Official Split', Core::TEXTDOMAIN ),
            'description' => __( 'Wirecard Brazil Official Payment Split', Core::TEXTDOMAIN ),
            'site'        => site_url(),
            'redirectUri' => $redirect_uri
        );

        $body = json_encode($data_body);
        $curl = curl_init();

        $options = array(
            CURLOPT_URL              => $notification_url,
            CURLOPT_RETURNTRANSFER   => true,
            CURLOPT_ENCODING         => "",
            CURLOPT_MAXREDIRS        => 10,
            CURLOPT_TIMEOUT          => 30,
            CURLOPT_HTTP_VERSION     => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST    => 'POST',
            CURLOPT_POSTFIELDS       => $body,
            CURLOPT_HTTPHEADER       => $header
        );

        curl_setopt_array($curl, $options);
        $response      = curl_exec($curl);
        $err           = curl_error($curl);
        $response_data = json_decode($response);

        curl_close($curl);

        if ( $err ) {
          echo "cURL Error #:" . $err;
        } 

        update_option( 'wirecard_split_app_id', base64_encode($response_data->id)) ;
        update_option( 'wirecard_split_accesstoken', base64_encode($response_data->accessToken) );
        update_option( 'wirecard_split_secret', base64_encode($response_data->secret) );
        update_option( 'wirecard_split_siteurl', base64_encode($response_data->website) );
        update_option( 'wirecard_split_redirecturl', base64_encode($response_data->redirectUri) );
    }

}