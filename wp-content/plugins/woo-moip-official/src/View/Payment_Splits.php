<?php
namespace Woocommerce\Moip\View;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Model\Payment_Split;

class Payment_Splits 
{
    public static function render_button_wirecard_split()
	{
		$split = new Payment_Split();
		
		ob_start();
		?>
		<table class='form-table'>
			<tr valign="top">
				<th><?php echo __( 'Split APP', Core::TEXTDOMAIN ); ?></th>
				<td class='forminp'>
					<form method="">
						<button class="webhook button" id="wirecard-split-app" name="wirecard_split_app"><?php echo __( 'Generate APP', Core::TEXTDOMAIN ); ?></button>
					</form>
					<p class="description "><?php echo __( 'Before generating the app enter Wirecard Token and Wirecard Key and save changes. These fields are <strong>REQUIRED</strong> to generate the app.', Core::TEXTDOMAIN ); ?></p>
				</td>
			</tr>
		</table>
		<?php

		if ( isset( $_POST['wirecard_split_app'] ) ) {
			$split->create_wirecard_split_app();
        }
        
		return ob_get_clean();
    }
    
    public static function render_info_wirecard_app()
    {
        $app_id          = base64_decode( get_option('wirecard_split_app_id') );
        $app_accesstoken = base64_decode( get_option('wirecard_split_accesstoken') );
		$app_secret      = base64_decode( get_option('wirecard_split_secret') );
		$app_siteurl     = base64_decode( get_option('wirecard_split_siteurl') );
		$app_redirecturl = base64_decode( get_option('wirecard_split_redirecturl') );

        if ( empty($app_id) && empty($app_accesstoken) && empty($app_secret) ) {
            $app_id          = __( 'not registered', Core::TEXTDOMAIN );
            $app_accesstoken = __( 'not registered', Core::TEXTDOMAIN );
			$app_secret      = __( 'not registered', Core::TEXTDOMAIN );
			$app_siteurl     = __( 'not registered', Core::TEXTDOMAIN );
			$app_redirecturl = __( 'not registered', Core::TEXTDOMAIN );
		}

		ob_start();

		?>
		<table class='form-table webhooks'>
			<tr>
		 		<th>APP ID:</th>
		 		<td class='forminp'>
		 			<?php echo $app_id; ?>
		 		</td>
		 	</tr>
            <tr>
	  	 		<th>AccessToken:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $app_accesstoken; ?>
	  	 		</td>
	  	 	</tr>
	  	 	<tr>
	  	 		<th>Secret:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $app_secret; ?>
	  	 		</td>
			</tr>
			<tr>
	  	 		<th>Site:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $app_siteurl; ?>
	  	 		</td>
			</tr>
			<tr>
	  	 		<th>Redirect url:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $app_redirecturl; ?>
	  	 		</td>
	  	 	</tr>      
		</table>
		<?php

		return ob_get_clean();
    }
}