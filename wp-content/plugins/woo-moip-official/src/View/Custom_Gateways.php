<?php
namespace Woocommerce\Moip\View;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Woocommerce\Moip\Core;
use Woocommerce\Moip\Helper\Utils;
use Woocommerce\Moip\Model\Custom_Gateway;

class Custom_Gateways 
{
	public static function render_notification_webhook( $settings )
	{
		$link             = Core::get_webhook_url();
		$notification_url = Utils::get_url_endpoint().'/v2/preferences/notifications';

		ob_start();

		?>
		<table class='form-table webhooks'>
			<tr>
		 		<th>Access Token:</th>
		 		<td class='forminp'>
		 			<?php echo $settings->authorize_data->accessToken; ?>
		 		</td>
		 	</tr>
	  	 	<tr>
	  	 		<th>URL:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $link; ?>
	  	 		</td>
	  	 	</tr>
	  	 	<tr>
	  	 		<th>ID:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $settings->webhook_id; ?>
	  	 		</td>
	  	 	</tr>
	  	 	<tr>
	  	 		<th>URL de Notificação:</th>
	  	 		<td class='forminp'>
	  	 			<?php echo $notification_url; ?>
	  	 		</td>
	  	 	</tr>
		</table>
		<?php

		return ob_get_clean();
	}

	public static function render_button_webhook_send()
	{
		$model  = new Custom_Gateway();
		
		ob_start();
		?>
		<table class='form-table'>
			<tr valign="top">
		 		<th><?php echo __( 'Status', Core::TEXTDOMAIN ); ?></th>
		 		<td class='forminp'>
		 			<form method="">
		 				<button class="webhook button" name="moip_send_webhook"><?php echo __( 'Update Notifications', Core::TEXTDOMAIN ); ?></button>
		 			</form>
		 			<p class="description "><?php echo __( 'Update notifications only if the WooCommerce / Wirecard status is not working.', Core::TEXTDOMAIN ); ?></p>
		 		</td>
		 	</tr>
		</table>

	<?php

		if ( isset( $_POST['moip_send_webhook'] ) ) {
			$model->delete_webhook_notification();
            $model->set_webhook_notification();
		}
		return ob_get_clean();
	}

}