<?php
/*
Plugin Name: Better VWO
Description: Better VWO plguin for optimizations
Author: Filip Kangalov - Quasar.
Version: 1.0
Author URI: https://quasar.mk
*/

//------------------------------------------------------------------------//
//---Hook-----------------------------------------------------------------//
//------------------------------------------------------------------------//
add_action( 'wp_enqueue_scripts', 'vwo_clhf_headercode');
add_action( 'admin_menu', 'vwo_clhf_plugin_menu' );
add_action( 'admin_init', 'vwo_clhf_register_mysettings' );
add_action( 'admin_notices','vwo_clhf_warn_nosettings');


//------------------------------------------------------------------------//
//---Functions------------------------------------------------------------//
//------------------------------------------------------------------------//
// options page link
function vwo_clhf_plugin_menu() {
  add_options_page('Visual Website Optimizer', 'Better VWO', 'create_users', 'clhf_vwo_options', 'vwo_clhf_plugin_options');
}

// whitelist settings
function vwo_clhf_register_mysettings(){
	register_setting('clhf_vwo_options','vwo_id','intval');
	register_setting('clhf_vwo_options','code_type');
	register_setting('clhf_vwo_options','settings_tolerance','intval');
	register_setting('clhf_vwo_options','library_tolerance','intval');
}

//------------------------------------------------------------------------//
//---Output Functions-----------------------------------------------------//
//------------------------------------------------------------------------//
function vwo_clhf_headercode(){	
	// runs in the header
	global $clhf_header_script_sync, $clhf_header_script_async;	
	$vwo_id = get_option('vwo_id');
	$code_type = get_option('code_type');
	$base_url = get_site_url();

	$time = time();

	if($vwo_id)
	{

		if($code_type == 'SYNC')
		{			
			wp_enqueue_script('syncvwojs', plugin_dir_url( __FILE__ ) . '/assets/sync.js', array(), $time);			
			
			//localize scripts
			wp_localize_script('syncvwojs', 'local', array(
		                  'ajax_url' => admin_url('admin-ajax.php'),
		                  'base_url' => $base_url,
		                  'plugin_dir_url' => plugin_dir_url(__FILE__),
		                  'vwo_id' => $vwo_id
		    ));
		}
		else 
		{
			$settings_tolerance = get_option('settings_tolerance');
			if(!is_numeric($settings_tolerance)) $settings_tolerance = 2000;

			$library_tolerance = get_option('library_tolerance');
			if(!is_numeric($library_tolerance)) $library_tolerance = 2500;

			wp_enqueue_script('asyncvwojs', plugin_dir_url( __FILE__ ) . '/assets/async.js', array('jquery'), $time);			
			
			//localize scripts
			wp_localize_script('asyncvwojs', 'alocal', array(
		                  'ajax_url' => admin_url('admin-ajax.php'),
		                  'base_url' => $base_url,
		                  'plugin_dir_url' => plugin_dir_url(__FILE__),
		                  'vwo_id' => $vwo_id,
		                  'settings_tolerance' => $settings_tolerance,
		                  'library_tolerance' => $library_tolerance,
		    ));

			
		}
	}
}
//------------------------------------------------------------------------//
//---Page Output Functions------------------------------------------------//
//------------------------------------------------------------------------//
// options page
function vwo_clhf_plugin_options() {
  echo '<div class="wrap">';?>
	<h2>VWO</h2>
	<p>You need to have a <a href="https://vwo.com/">VWO</a> account in order to use this plugin. This plugin inserts the neccessary code into your Wordpress site automatically without you having to touch anything. In order to use the plugin, you need to enter your VWO Account ID:</p>
	<form method="post" action="options.php">
	<?php settings_fields( 'clhf_vwo_options' ); ?>
	<table class="form-table">
        <tr valign="top">
            <th scope="row">Your VWO Account ID</th>
            <td><input type="text" name="vwo_id" value="<?php echo get_option('vwo_id'); ?>" /></td>
        </tr>
		<tr valign="top">
	        <th scope="row">Code (default: Asynchronous)</th>
	        <td>
		        <input style="vertical-align: text-top;" type="radio" onclick="selectCodeType();" name="code_type" id="code_type_async" value="ASYNC" <?php if(get_option('code_type')!='SYNC') echo "checked"; ?> /> Asynchronous&nbsp;&nbsp;&nbsp;
		        <input style="vertical-align: text-top;" type="radio" onclick="selectCodeType();" name="code_type" id="code_type_sync" value="SYNC" <?php if(get_option('code_type')=='SYNC') echo "checked"; ?> /> Synchronous
		        &nbsp;<a href="https://vwo.com/blog/asynchronous-code" target="_blank">[Help]</a>
	        </td>
        </tr>
		<tr valign="top" id='asyncOnly1' <?php if(get_option('code_type')=='SYNC') echo "style='display:none;'" ?>>
	        <th scope="row">Settings Timeout</th>
			<td style="vertical-align: middle;"><input type="text" name="settings_tolerance" value="<?php echo get_option('settings_tolerance')?get_option('settings_tolerance'):2000; ?>" />ms  (default: 2000)</td>
	    </tr>
		<tr valign="top" id='asyncOnly2' <?php if(get_option('code_type')=='SYNC') echo "style='display:none;'" ?>>
	        <th scope="row">Library Timeout</th>
			<td style="vertical-align: middle;"><input type="text" name="library_tolerance" value="<?php echo get_option('library_tolerance')?get_option('library_tolerance'):2500; ?>" />ms  (default: 2500)</td>
	    </tr>
	</table>

	<script type="text/javascript">
		function selectCodeType() {
			var code_type = 'ASYNC';
			if(document.getElementById('code_type_sync').checked)
				code_type = 'SYNC';

			if(code_type == 'ASYNC') {
				document.getElementById('asyncOnly1').style.display = 'table-row';
				document.getElementById('asyncOnly2').style.display = 'table-row';
			}
			else {
				document.getElementById('asyncOnly1').style.display = 'none';
				document.getElementById('asyncOnly2').style.display = 'none';
			}
		}
	</script>
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" /></p>
	<p>Your Account ID (a number) can be found in <i>Settings</i> area (top-right) after you <a href="https://app.vwo.com">login</a> into your VWO account.</p>
<?php
  echo '</div>';
}

function vwo_clhf_warn_nosettings(){
    if (!is_admin())
        return;

  $clhf_option = get_option("vwo_id");
  if (!$clhf_option || $clhf_option < 1){
    echo "<div id='vwo-warning' class='updated fade'><p><strong>VWO is almost ready.</strong> You must <a href=\"options-general.php?page=clhf_vwo_options\">enter your Account ID</a> for it to work.</p></div>";
  }
}
?>